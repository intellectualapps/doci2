//
//  GridSectionController.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/22/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
import IGListKit

protocol GridSectionControllerDelegate: NSObjectProtocol {
    func didSelectItem(item: Any?)
}

class GridSectionController: ListSectionController {
    weak var delegate: GridSectionControllerDelegate?
    var object: GridItem?
    let isReorderable: Bool

    required init(isReorderable: Bool = false) {
        self.isReorderable = isReorderable
        super.init()
        self.minimumInteritemSpacing = 10
        self.minimumLineSpacing = 10
        self.inset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        setInset()
    }

    func setInset() {
        self.inset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }

    override func numberOfItems() -> Int {
        return object?.items.count ?? 0
    }

    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: self.itemWidth(), height: itemHeight())
    }

    func itemHeight() -> CGFloat {
        let width = contextWidth()
        let itemSize = floor(width / 2)
        return itemSize
    }

    func itemWidth() -> CGFloat {
        let width = contextWidth()
        let itemSize = floor(width / 2)
        return itemSize
    }

    func contextWidth() -> CGFloat {
        let width = (collectionContext?.containerSize.width ?? 0) - 10 - self.inset.left - self.inset.right
        return width
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        fatalError("Need override to provide correct collection cell")
    }

    override func didUpdate(to object: Any) {
        self.object = object as? GridItem
    }

    override func canMoveItem(at index: Int) -> Bool {
        return isReorderable
    }

    override func moveObject(from sourceIndex: Int, to destinationIndex: Int) {
        guard let object = object else { return }
        let item = object.items.remove(at: sourceIndex)
        object.items.insert(item, at: destinationIndex)
    }

    override func didSelectItem(at index: Int) {
        guard let object = object else { return }
        if index < object.items.count {
            self.delegate?.didSelectItem(item: object.items[index])
        }
    }
}
