//
//  VCellInfo.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/21/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class VCellInfo: NSObject {
    var reuseIdentifier = ""
    var id = -1
    var height: CGFloat = 0
    var content: Any?

    convenience init(reuseIdentifier: String = "",
                     id: Int = -1,
                     height: CGFloat = 0,
                     content: Any? = nil) {
        self.init()
        self.reuseIdentifier = reuseIdentifier
        self.id = id
        self.height = height
        self.content = content
    }
}
