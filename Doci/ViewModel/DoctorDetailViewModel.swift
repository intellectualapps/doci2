//
//  DoctorDetailViewModel.swift
//  
//
//  Created by Nguyen Van Dung on 7/25/19.
//

import Foundation


class DoctorDetailViewModel: NSObject {
    var doctor: Doctor?
    var sections = [VSection]()

    func prepare() {
        sections.removeAll()
        if let d = doctor {
            let infos = d.toDetailContents()
            let sInfo = VSection(reuseIdenfier: "", height: 0.1, sectionId: nil, contentObj: nil) { () -> ([VCellInfo]) in
                var cells = [VCellInfo]()
                if infos.count > 0 {
                    infos.forEach { (content) in
                        if let _ = content.content as? VLocation {
                            let cell = VCellInfo(reuseIdentifier: "DetailLocationCell", id: -1, height: 0, content: content)
                            cells.append(cell)
                        } else {
                            let cell = VCellInfo(reuseIdentifier: "DoctorDetailCell", id: -1, height: 0, content: content)
                            cells.append(cell)
                        }
                    }
                }
                return cells
            }
            sections.append(sInfo)
        } else {
            sections = []
        }
    }
}
