//
//  GridViewViewModel.swift
//  CourseApp
//
//  Created by Nguyen Van Dung on 1/27/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class GridViewViewModel: NSObject {
    //Contains grid item. Each item per 1 section
    var sections = Variable([GridItem]())

    func prepareDataSource() {
        fakeData()
    }

    func getDatas() -> [GridItemInfo] {
        return []
    }

    func fakeData() {
        let grid = GridItem(items: getDatas())
        self.sections.value = [grid]
    }
}
