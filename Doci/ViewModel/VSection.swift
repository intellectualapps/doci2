//
//  VSection.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/21/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class VSection: NSObject {
    var reuseIdentifier = ""
    var id = -1
    var height: CGFloat = 0
    var content: Any?
    var cells = [VCellInfo]()

    convenience init(reuseIdenfier: String = "",
                     height: CGFloat = 0.1,
                     sectionId: Int? = nil,
                     contentObj: Any? = nil, constructor: () -> ([VCellInfo])) {
        self.init()
        self.reuseIdentifier = reuseIdenfier
        self.height = height
        self.cells = constructor()
        self.id = sectionId ?? -1
        self.content = contentObj
    }
}
