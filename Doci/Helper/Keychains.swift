//
//  Keychains.swift
//  Jgrammar
//
//  Created by Nguyen Van Dung on 12/11/16.
//  Copyright © 2016 Dht. All rights reserved.
//

import Foundation

enum Keychains: String {
    case purchasedid = "purchasedid"
    case deviceId = "cplDeviceUdid"
    case passcode = "passcode"
    case accesstoken = "accessToken"

    func set(value: Any?) {
        if let value = value {
            KeychainItemWrapper.save(self.rawValue, data: value)
        }
    }

    func get() -> Any? {
        return KeychainItemWrapper.load(self.rawValue)
    }

    func getStringOrEmpty() -> String {
        if let value = KeychainItemWrapper.load(self.rawValue) as? String {
            return value
        }
        return ""
    }

    static func getDeviceId() -> String {
        var dvId = Keychains.deviceId.getStringOrEmpty()
        if dvId.isEmpty {
            dvId = UIDevice.current.identifierForVendor?.uuidString ?? ""
        }
        Keychains.deviceId.set(value: dvId)
        return dvId
    }

    static func savePurchasedProduct(productid: String, transactionid: String) {
        KeychainItemWrapper.save(productid, data: transactionid)
    }

    static func getTransactionIdForProductId(productid: String) -> String? {
        if let obj = KeychainItemWrapper.load(productid) as? String {
            if obj.count > 0 {
                //Obj is transactionid
                return obj
            }
        }
        return nil
    }

    static func didBuyForProduct(productid: String) -> (Bool,String) {
        if let obj = KeychainItemWrapper.load(productid) as? String {
            if obj.count > 0 {
                //Obj is transactionid
                return (true, obj)
            }
        }
        return (false, "")
    }
}
