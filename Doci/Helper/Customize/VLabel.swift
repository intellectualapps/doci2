//
//  VLabel.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/16/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
@IBDesignable
class VLabel: VLocalizeLabel {
    @IBInspectable public var fontSize: CGFloat = 16 {
        didSet {
            updateFont()
        }
    }

    @IBInspectable public var fontType: Int = 0 {
        didSet {
            updateFont()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        updateFont()
    }

    func updateFont() {
        if let type = FontType(rawValue: fontType) {
            let font = UIFont.defaultFont(style: type, size: fontSize)
            self.font = font
        }
    }
}
