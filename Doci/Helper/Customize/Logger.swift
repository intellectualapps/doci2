//
//  Logger.swift
//  SakuraTsushin
//
//  Created by Nguyen Van Dung on 1/3/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class Logger: NSObject {
    class func log(info: Any, force: Bool = false) {
        print(info)
    }
}
