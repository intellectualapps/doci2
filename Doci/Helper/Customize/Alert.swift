//
//  Alert.swift
//  MobileWarehouse
//
//  Created by Nguyen Van Dung on 5/2/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation

/**
 * This is Alert helper class. Will display alert view on screen and handle action callback
 */
class Alert: NSObject {

    /*
     Will show alert if have error infor
     */
    class func showAlertIfNeed(_ msgErr: String?, interactObject: AnyObject?, completion:(()->())? = nil) {
        if let err = msgErr, err.count > 0 {
            Alert.showErrorMessageAlert(err, interactObject: interactObject, completion: completion)
        }
    }

    class func showAlertWithErrorMessage(_ errMsg: String, title: String? = nil, completion: (()->())? = nil) {
        Alert.showAlertWithErrorMessage(errMsg, title: title, cancelBtnTitle: nil, completion: completion)
    }

    class func showAlertWithErrorMessage(_ errMsg: String, title: String? = nil, cancelBtnTitle: String?, completion: (()->())? = nil) {
        DispatchQueue.main.async {
            if WindowManager.shared.alertWindow.isHidden == false {
                return
            }
            var cancelTitle = cancelBtnTitle
            if (cancelTitle == nil) {
                cancelTitle = "button.close".localized()
            }
            let alertController = UIAlertController(title: title ?? "", message: errMsg, preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
                completion?()
                WindowManager.shared.alertWindow.isHidden = true
            })
            alertController.addAction(action)
            let rootController = WindowManager.shared.alertWindow.rootViewController
            WindowManager.shared.alertWindow.isHidden = false
            rootController?.present(alertController, animated: true, completion: nil)
        }
    }

    /**
     * This will show alert view with 2 buttons (Ok, Cancel). It is help full incase need confirm
     */
    class func showConfirmAlert(_ title: String = "", message: String, cancelBtnTitle: String, okBtnTitle: String, completion: @escaping (Bool)->()) {
        DispatchQueue.main.async {
            if WindowManager.shared.alertWindow.isHidden == false {
                return
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title: cancelBtnTitle, style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
                completion(false)
                WindowManager.shared.alertWindow.isHidden = true
            })
            alertController.addAction(action)

            let okAction = UIAlertAction(title: okBtnTitle, style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                completion(true)
                WindowManager.shared.alertWindow.isHidden = true
            })
            alertController.addAction(okAction)
            let rootController = WindowManager.shared.alertWindow.rootViewController
            WindowManager.shared.alertWindow.isHidden = false
            rootController?.present(alertController, animated: true, completion: nil)
        }
    }

    class func showConfirmAlert(_ title: String = "", message: String, cancelBtnTitle: String, actions: [String], completion: @escaping (_ action: String)->()) {
        DispatchQueue.main.async {
            if WindowManager.shared.alertWindow.isHidden == false {
                return
            }
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title: cancelBtnTitle, style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
                completion(cancelBtnTitle)
                WindowManager.shared.alertWindow.isHidden = true
            })
            alertController.addAction(action)
            if actions.count > 0 {
                for i in 0..<actions.count {
                    let action = actions[i]
                    let okAction = UIAlertAction(title: action, style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                        completion(action)
                        WindowManager.shared.alertWindow.isHidden = true
                    })
                    alertController.addAction(okAction)
                }
            }

            let rootController = WindowManager.shared.alertWindow.rootViewController
            WindowManager.shared.alertWindow.isHidden = false
            rootController?.present(alertController, animated: true, completion: nil)
        }
    }

    class func showErrorMessageAlert(_ message: String, interactObject: AnyObject?, completion:(()->())? = nil) {
        if WindowManager.shared.alertWindow.isHidden == false {
            return
        }
        let alertVC = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alertVC.addAction(UIAlertAction(title: NSLocalizedString("button.close", comment: ""), style: UIAlertAction.Style.cancel, handler: { (action) -> Void in
            completion?()
            WindowManager.shared.alertWindow.isHidden = true
        }))
        let rootController = WindowManager.shared.alertWindow.rootViewController
        WindowManager.shared.alertWindow.isHidden = false
        rootController?.present(alertVC, animated: true, completion: nil)
    }
}
