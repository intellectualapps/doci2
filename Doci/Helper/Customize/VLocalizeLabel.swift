//
//  VLocalizeLabel.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/26/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
@IBDesignable
class VLocalizeLabel: UILabel {
    @IBInspectable public var localizeKey: String = "" {
        didSet {
            localize()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        localize()
    }

    func localize() {
        if localizeKey.count > 0 {
            self.text = localizeKey.localized()
        }
    }
}
