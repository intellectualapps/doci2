//
//  VImageView.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/28/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class VImageView: UIImageView {
    /**
     *  Setup corner radius for layer
     */
    @IBInspectable internal var cornerRadius: CGFloat = 0 {
        didSet {
            if self.cornerRadius > 0 {
                self.layer.cornerRadius = cornerRadius
                self.layer.masksToBounds = true
            }
        }
    }

    @IBInspectable internal var borderWidth: CGFloat = 0.5 {
        didSet {
            if self.borderWidth > 0 {
                self.layer.borderWidth = borderWidth
                self.layer.masksToBounds = true
            }
        }
    }

    @IBInspectable internal var borderColor: UIColor? = nil {
        didSet {
            if let color = self.borderColor {
                self.layer.borderColor = (color ).cgColor
                self.layer.masksToBounds = true
            }
        }
    }
}
