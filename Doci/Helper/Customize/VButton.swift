//
//  VButton.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/17/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

@IBDesignable
class VButton: UIButton {
    @IBInspectable internal var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
        }
    }

    @IBInspectable internal var borderWidth: CGFloat = 0.5 {
        didSet {
            self.layer.borderWidth = borderWidth
            self.layer.masksToBounds = true
        }
    }

    @IBInspectable internal var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = (borderColor ).cgColor
            self.layer.masksToBounds = true
        }
    }
    
    var extendedEdges: UIEdgeInsets = .zero
    @IBInspectable public var localizeKey: String = "" {
        didSet {
            localize()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        localize()
    }

    func localize() {
        if localizeKey.count > 0 {
            self.setTitle(localizeKey.localized(), for: .normal)
        }
    }

    @IBInspectable public var fontSize: CGFloat = 16 {
        didSet {
            updateFont()
        }
    }

    @IBInspectable public var fontType: Int = 0 {
        didSet {
            updateFont()
        }
    }

    func updateFont() {
        if let type = FontType(rawValue: fontType) {
            let font = UIFont.defaultFont(style: type, size: fontSize)
            self.titleLabel?.font = font
        }
    }

    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let newBound = CGRect(
            x: self.bounds.origin.x - extendedEdges.left,
            y: self.bounds.origin.y - extendedEdges.top,
            width: self.bounds.width +  extendedEdges.left +  extendedEdges.right,
            height: self.bounds.height + extendedEdges.top +  extendedEdges.bottom
        )
        return newBound.contains(point)
    }
}
