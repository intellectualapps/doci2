//
//  LocationManager.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/25/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
import INTULocationManager
private let kDefaultCoordinateValue: Float = -1.00

class CoreLocation: NSObject {
    static let shared = CoreLocation()

    var address = ""
    var city = ""
    var street = ""
    var latitude = NSNumber(value: kDefaultCoordinateValue as Float)
    var longitude = NSNumber(value: kDefaultCoordinateValue as Float)
    var locationService: CLLocationManager?
    var geocoder: CLGeocoder?
    var currentLocation: Variable<VLocation?> = Variable(nil)
    var observerID: UInt64 = 0

    fileprivate override init() {
        self.locationService = CLLocationManager()
        self.geocoder = CLGeocoder()
        super.init()
    }

    func prepare() {
        LocationManager.shared.requireUserAuthorization()
        observerID = LocationManager.shared.onAuthorizationChange.add { (state) in
            print("Authorization status changed to \(state)")
        }
    }

    func startLocation(_ success:@escaping (_ location: CLLocation?) ->Void, failure:@escaping () ->Void) {
        let manager = INTULocationManager.sharedInstance()
        manager.requestLocation(
            withDesiredAccuracy: .city,
            timeout: 10,
            delayUntilAuthorized: true,
            block: {currentLocation, achievedAccuracy, status in
                switch status {
                case .success:
                    self.latitude = NSNumber(value: (currentLocation?.coordinate.latitude)! as Double)
                    self.longitude = NSNumber(value: (currentLocation?.coordinate.longitude)! as Double)
                    self.locationService?.delegate = self
                    self.locationService?.startUpdatingLocation()
                    success(currentLocation)
                    break
                case .timedOut:
                    //Alert.showAlertWithErrorMessage("Location request timed out. Current Location:\(String(describing: currentLocation))")
                    failure()
                    break
                default:
                    //Alert.showAlertWithErrorMessage(self.getLocationErrorDescription(status))
                    failure()
                    break
                }
        })
    }

    func getLocationErrorDescription(_ status: INTULocationStatus) -> String {
        if status == .servicesNotDetermined {
            return "Error: User has not responded to the permissions alert."
        }
        if status == .servicesDenied {
            return "Error: User has denied this app permissions to access device location."
        }
        if status == .servicesRestricted {
            return "Error: User is restricted from using location services by a usage policy."
        }
        if status == .servicesDisabled {
            return "Error: Location services are turned off for all apps on this device."
        }
        return "An unknown error occurred.\n(Are you using iOS Simulator with location set to 'None'?)"
    }

    /**
     If locate failed or has an error, reset all values
     */
    func resetLocation() {
        self.address = ""
        self.city = ""
        self.street = ""
        self.latitude = NSNumber(value: kDefaultCoordinateValue as Float)
        self.longitude = NSNumber(value: kDefaultCoordinateValue as Float)
    }

    /**
     Update the user's location

     - parameter userLocation: CLLocation
     */
    func updateLocation(_ userLocation: CLLocation) {
        self.latitude = NSNumber(value: userLocation.coordinate.latitude as Double)
        self.longitude = NSNumber(value: userLocation.coordinate.longitude as Double)
        if let geocoder = self.geocoder {
            if !geocoder.isGeocoding {
                geocoder.reverseGeocodeLocation(userLocation, completionHandler: {[weak self] (placemarks, error) in
                    if let error = error {
                        print("reverse geodcode fail: \(error.localizedDescription)")
                        return
                    }
                    if let first = placemarks?.first {
                        let admin = first.administrativeArea ?? ""
                        let sub = first.subLocality ?? ""
                        let thorough = first.thoroughfare ?? ""
                        self?.address = admin + "," + sub + "," + thorough
                        self?.city = admin
                        self?.street = thorough
                        print("======>>> ADDRESSS <<<=========")
                        print(self?.address)
                    }
                })
            }
        }
    }

    func getFullLocationInfoForCLLocation(location: CLLocation, completion:@escaping (VLocation?) -> ()) {
        if let geocoder = self.geocoder {
            geocoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
                var vLocation: VLocation?
                if let first = placemarks?.first {
                    vLocation = VLocation()
                    vLocation?.coordinate = location.coordinate

                    let admin = first.administrativeArea ?? ""
                    let sub = first.subLocality ?? ""
                    let thorough = first.thoroughfare ?? ""
                    let address = admin + "," + sub + "," + thorough
                    let city = admin
                    vLocation?.fullAddress = address
                    vLocation?.city = city
                    vLocation?.area = admin
                }
                completion(vLocation)
            })
        } else {
            completion(nil)
        }
    }

    func getCurrentLocation(completion: ((VLocation?) -> ())? = nil) {
        self.startLocation({[weak self] (clLocation) in
            if let location = clLocation {
                self?.getFullLocationInfoForCLLocation(location: location, completion: { (vlocation) in
                    self?.currentLocation.value = vlocation
                    self?.locationService?.stopUpdatingLocation()
                    completion?(vlocation)
                })
            } else {
                self?.locationService?.stopUpdatingLocation()
                completion?(nil)
            }
        }) {
            self.locationService?.stopUpdatingLocation()
            completion?(nil)
        }
    }
}


// MARK: - @delegate CLLocationManagerDelegate
extension CoreLocation: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let last = locations.last {
            self.updateLocation(last)
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        self.updateLocation(manager.location!)
    }

    func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?) {
        self.resetLocation()
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.resetLocation()
       // Alert.showAlertWithErrorMessage("\(error.localizedDescription)")
    }
}
