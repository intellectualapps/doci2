//
//  UIViewExtension.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 5/22/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    var heightConstaint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .height && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }

    var widthConstaint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .width && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    func removeAllGestures() {
        if let gestures = self.gestureRecognizers {
            gestures.forEach { (gt) in
                self.removeGestureRecognizer(gt)
            }
        }
    }

    func addAction(target: Any, action: Selector) {
        self.removeAllGestures()
        self.isUserInteractionEnabled = true
        let tap = UIGestureRecognizer(target: target, action: action)
        self.addGestureRecognizer(tap)
    }

    class func fromNib<T: UIView>(nibNameOrNil: String? = nil) -> T {
        let v: T? = fromNib(nibNameOrNil: nibNameOrNil)
        return v!
    }

    //Initialize instance from nib file
    class func fromNib<T: UIView>(nibNameOrNil: String? = nil) -> T? {
        var view: T?
        var name = ""
        if let nibName = nibNameOrNil {
            name = nibName
        } else {
            name = "\(T.self)".components(separatedBy: ".").last ?? ""
        }
        if let nibViews = Bundle.main.loadNibNamed(name, owner: nil, options: nil) {
            for v in nibViews {
                if let tog = v as? T {
                    view = tog
                }
            }
        }
        return view
    }

    func roundCorner(radius: CGFloat, borderColor: UIColor = UIColor.white, borderWidth: CGFloat = 1) {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }

    func constraintFor(attribute: NSLayoutConstraint.Attribute) -> NSLayoutConstraint? {
        let constaints = self.constraints
        for ct in constaints {
            if ct.firstAttribute == attribute {
                return ct
            }
        }
        return nil
    }

    public func removeAllSubviews() {
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }

    func removeRoundBorderColor() {
        for view in self.subviews {
            let radius = view.layer.cornerRadius
            if radius > 0 {
                view.layer.borderColor = UIColor.clear.cgColor
            } else {
                view.removeRoundBorderColor()
            }
        }
    }

    func roundView() {
        let minSize = min(bounds.width, bounds.height)
        layer.cornerRadius = minSize / 2
        layer.masksToBounds = true
    }

    func clearTableHeaderFooterBackground() {
        if let view = self as? UITableViewHeaderFooterView {
            view.backgroundView?.backgroundColor = .clear
            view.contentView.backgroundColor = .clear
        }
    }
}
