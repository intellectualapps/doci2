//
//  UIFont+Ext.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/16/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
enum FontType: Int {
    case Regular = 0
    case LightItalic = 1
    case Medium = 2
    case BoldItalic = 3
    case Light = 4
    case ThinItalic = 5
    case ExtraLight = 6
    case Thin = 7
    case Bold = 8
    case MediumItalic = 9
    case BlackItalic = 10
    case SemiBold = 11
    case ExtraLightItalic = 12
    case ExtraBold = 13
    case Black
    case Italic
    case SemiBoldItalic
    case ExtraBoldItalic
    var name: String {
        let names = UIFont.fontName()
        if self.rawValue < names.count {
            let aname = names[self.rawValue]
            let newname = (aname as NSString).deletingPathExtension
            return newname
        }
        return ""
    }
}

extension UIFont {
    static func defaultFont(style: FontType = .Regular,
                            size: CGFloat = 16) -> UIFont {
        guard let font = UIFont(name: style.name, size: size) else {
            return UIFont.systemFont(ofSize: size)
        }
        return font
    }

    func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }

    class func fontName() -> [String] {
        return [
            "Montserrat-Regular.ttf",
            "Montserrat-LightItalic.ttf",
            "Montserrat-Medium.ttf",
            "Montserrat-BoldItalic.ttf",
            "Montserrat-Light.ttf",
            "Montserrat-ThinItalic.ttf",
            "Montserrat-ExtraLight.ttf",
            "Montserrat-Thin.ttf",
            "Montserrat-Bold.ttf",
            "Montserrat-MediumItalic.ttf",
            "Montserrat-BlackItalic.ttf",
            "Montserrat-SemiBold.ttf",
            "Montserrat-ExtraLightItalic.ttf",
            "Montserrat-ExtraBold.ttf",
            "Montserrat-Black.ttf",
            "Montserrat-Italic.ttf",
            "Montserrat-SemiBoldItalic.ttf",
            "Montserrat-ExtraBoldItalic.ttf"]
    }

    class func loadCustomFont() {
        var paths = [String]()
        let fontFileNames = UIFont.fontName()

        for name in fontFileNames {
            let filename = (name as NSString).deletingPathExtension
            let fExtension = (name as NSString).pathExtension
            if let path = Bundle.main.path(forResource: filename, ofType: fExtension) {
                paths.append(path)
            }
        }
        loadCustomFont(filePaths: paths)
    }

    class func loadCustomFont(filePaths: [String]) {
        for path in filePaths {
            if FileManager.default.fileExists(atPath: path) {
                let url = URL(fileURLWithPath: path)
                do {
                    let data = try Data(contentsOf: url)
                    if let provider = CGDataProvider(data: data as CFData) {
                        if let font = CGFont(provider) {
                            CTFontManagerRegisterGraphicsFont(font, nil)
                        }
                    }
                } catch {

                }
            }
        }
    }
}
