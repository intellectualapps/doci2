//
//  Double+Ext.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 5/19/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

extension Double {
    func moneyText() -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: self as NSNumber) {
            return formattedTipAmount
        }
        return ""
    }

    func toKm() -> String {
        print(self)
        if self >= 1000 {
            return String.init(format: "%0.1f", self / 1000) + " km"
        } else {
            return String.init(format: "%d", self) + " m"
        }
    }

    func startTime() -> String {
        let date = Date(timeIntervalSince1970: self)
        let str = date.toString(DateToStringStyles.custom("HH:mm:ss"))
        let info = str.toInterval().secondsToHoursMinutesSeconds()
        if info.0 > 12 {
            return String.init(format: "%02i:%02i", info.0 - 12, info.1) + " PM"
        } else {
            return String.init(format: "%02i:%02i", info.0, info.1) + " AM"
        }
    }
}
