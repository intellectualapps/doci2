//
//  UILabel+Ext.h
//  YogaDatabase
//
//  Created by Nguyen Van Dung on 5/4/18.
//

#import <UIKit/UIKit.h>

@interface UILabel(Ext)
- (CGRect)boundingRectForCharacterRange:(NSRange)range;
@end
