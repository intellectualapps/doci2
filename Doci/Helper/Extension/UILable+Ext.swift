//
//  UILable+Ext.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/16/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
public extension UILabel {
    convenience init(text: String?) {
        self.init()
        self.text = text
    }

    var requiredHeight: CGFloat {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.attributedText = attributedText
        label.sizeToFit()
        return label.frame.height
    }

    func rangeOfText(txt: String) -> NSRange {
        guard let text = text else { return NSMakeRange(0, 0)}
        var underlineRange = NSMakeRange(0, 0)
        if text.contains(txt) {
            underlineRange = (text as NSString).range(of: txt, options: NSString.CompareOptions.caseInsensitive)
        }
        return underlineRange
    }

    func makeUnderline(underlineText: String) -> NSRange {
        guard let text = text else { return NSMakeRange(0, 0)}
        let underlineRange = self.rangeOfText(txt: underlineText)
        if underlineRange.length > 0 {
            let attributedText = NSMutableAttributedString(string: text)
            attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: underlineRange)
            // Add other attributes if needed
            self.attributedText = attributedText
        }
        return underlineRange
    }
}
