//
//  UIViewController+Ext.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/17/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
import GoogleMaps

extension UIViewController: GIDSignInDelegate, GIDSignInUIDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    func loginGoogle(completion: @escaping LoginGoogleCompletion) {
        GUIHelper.shared.loginGoogleCompletion = completion
        let signin = GIDSignIn.sharedInstance()
        signin?.shouldFetchBasicProfile = true
        signin?.clientID = googleClientId
        signin?.scopes = ["profile"]
        signin?.delegate = self
        signin?.uiDelegate = self
        signin?.signIn()
    }

    public func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        if let data = signIn?.currentUser?.profile  {
            print(data.email)
        }
    }

    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
            Alert.showAlertWithErrorMessage(error.localizedDescription)
            GUIHelper.shared.loginGoogleCompletion?(nil)
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile?.name ?? ""
            let email = user.profile?.email ?? ""
            let photo = user.profile?.imageURL(withDimension: 300)?.absoluteString ?? ""
            let info = SocialInfo(name: fullName, email: email, token: idToken ?? "", id: userId ?? "", photo: photo)
            GUIHelper.shared.loginGoogleCompletion?(info)

            // ...
        }
    }
    func callLoginGoogle(info: SocialInfo, completion: @escaping NetworkServiceCompletion) {
        WindowManager.shared.showProgressView()
        LoginGoogleService.loginGoogle(name: info.name, email: info.email, token: info.token, photo: info.photo, photoSmall: info.photo, completion: { (response, error) in
            WindowManager.shared.hideProgressView()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                AppDelegate.shared.showMainScreen()
            }
            completion(response, error)
        })
    }

    func callFacebookLogin(info: SocialInfo, completion: @escaping NetworkServiceCompletion) {
        LoginManager().logOut()
        WindowManager.shared.showProgressView()
        LoginFacebookService.loginFacebook(name: info.name, email: info.email, token: info.token, photo: info.photo, photoSmall: info.photo, completion: { (response, error) in
            WindowManager.shared.hideProgressView()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                AppDelegate.shared.showMainScreen()
            }
            completion(response, error)
        })
    }

    func loginFacebookAndGetInfo(completion: @escaping (_ fbinfo: SocialInfo?) -> ()) {

        func getFacebookInfo(token: String, completion: @escaping (_ fbinfo: SocialInfo?) -> ()) {
            self.requestFacebookInfo(token: token, completion: { (fbInfo) in
                if let info = fbInfo {
                    completion(info)
                } else {
                    completion(nil)
                }
            })
        }
        //1. check current login status
        let loginManager = LoginManager()
        if let token = AccessToken.current?.tokenString {
            getFacebookInfo(token: token, completion: completion)
        } else {
            let permission = ["email", "public_profile"]
            loginManager.logIn(permissions: permission, from: self) {[weak self] (result, error) in
                if let token = result?.token?.tokenString {
                    getFacebookInfo(token: token, completion: completion)
                } else {
                    completion(nil)
                }
            }
        }
    }

    func requestFacebookInfo(token: String, completion: @escaping (_ fbinfo: SocialInfo?) -> ()) {
        let req = GraphRequest(graphPath: "me", parameters: ["fields":"id,email,last_name,first_name,picture"], tokenString: token, version: nil, httpMethod: HTTPMethod(rawValue: "GET"))
        req.start(completionHandler: { (connection, result, error) in

            if let dict = result as? [String: Any] {
                print(dict)
                var name = dict.stringOrEmptyForKey(key: "name")
                let email = dict.stringOrEmptyForKey(key: "email")
                let id = dict.stringOrEmptyForKey(key: "id")
                let photo = "http://graph.facebook.com/\(id)/picture?type=large"
                let lastname = dict.stringOrEmptyForKey(key: "last_name")
                let fname = dict.stringOrEmptyForKey(key: "first_name")
                if name.count == 0 {
                    name = fname + " " + lastname
                }
                let info = SocialInfo(name: name, email: email, token: token, id: id, photo: photo)
                completion(info)
            } else {
                completion(nil)
            }
        })
    }
    
    func showTip(data: Any?, target: UIView) {
        if GUIHelper.shared.tipView != nil {
            return
        }
        print(NSStringFromClass(self.classForCoder) + "." + #function)
        guard let tipview = TipView.newInstance() else {
            return
        }
        GUIHelper.shared.tipView = tipview
        let frame = self.view.convert(target.frame, from: target)
        let popTip = PopTip()
        popTip.padding = 20
        tipview.popTip = popTip
        tipview.frame = CGRect(x: 0, y: 0, width: 250, height: 150)
        popTip.show(customView: tipview,
                    direction: .up,
                    in: self.view,
                    from: frame)
        popTip.shouldDismissOnTap = false
        popTip.tapOutsideHandler = {[weak self] (pop) in
            popTip.hide()
            GUIHelper.shared.tipView = nil
        }
        popTip.tapHandler = { [weak self] (pop) in

        }

    }

    func showLeftMenu(isShow: Bool = true) {
        if let menu = GUIHelper.shared.leftMenuController {
            if isShow {
                self.present(menu, animated: true, completion: nil)
            } else {
                menu.dismiss(animated: true, completion: nil)
            }
        }
    }

    func showNavigation(isShow: Bool) {
        self.navigationController?.setNavigationBarHidden(!isShow, animated: false)
        self.tabBarController?.navigationController?.setNavigationBarHidden(!isShow, animated: false)
    }


    func presentTransperant(_ viewControllerToPresent: UIViewController, animated: Bool, completion: (() -> Void)? = nil) {
        viewControllerToPresent.modalPresentationStyle = .overCurrentContext
        viewControllerToPresent.modalTransitionStyle = .crossDissolve

        viewControllerToPresent.view.backgroundColor = UIColor(hex: 0x000000, alpha: 0.4)
        present(viewControllerToPresent, animated: animated, completion: completion)
    }

    func disableSwipeBack() {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    func enableSwipeBack() {
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }

    func changeNavigationColor(tintcolor: UIColor, textColor: UIColor) {
        navigationController?.navigationBar.barTintColor = tintcolor
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: textColor]

    }

    func showGenderFilter(lastOption: Int, completion: @escaping (_ newOption: Int) -> ()) {
        if let controller = UIStoryboard.main().FilterGenderVC as? FilterGenderVC {
            controller.lastFilterOption = lastOption
            controller.completion = completion
            let popup = PopupDialog(viewController: controller, buttonAlignment: NSLayoutConstraint.Axis.vertical, transitionStyle: .zoomIn, preferredWidth: 280, tapGestureDismissal: true, panGestureDismissal: true, hideStatusBar: true) {

            }
            self.present(popup, animated: true, completion: nil)
        }
    }
}
