//
//  UIImage+Ext.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 7/17/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

extension UIImage {
    class func hotelPlaceholder() -> UIImage? {
        return UIImage.init(named: "hotel_placeholder")
    }

    class func placePlaceholder() -> UIImage? {
        return UIImage.init(named: "place_placeholder")
    }

    class func restaurantPlaceholder() -> UIImage? {
        return UIImage.init(named: "restaurant_placeholder")
    }
}
