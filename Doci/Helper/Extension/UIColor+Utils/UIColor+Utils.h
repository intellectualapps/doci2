//
//  UIColor+Utils.h
//  zipGo
//
//  Created by msahaydak on 3/31/16.
//  Copyright © 2016 zipGo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Utils)
+ (UIColor *)colorWithHex:(UInt32)col;
+ (UIColor *)colorWithHexString:(NSString *)str;
+ (UIColor *)colorWithHex:(UInt32)col alpha:(CGFloat)alpha;
@end
