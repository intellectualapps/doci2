//
//  Date+Ext.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/26/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
extension DateFormatter {

    enum Format: String {
        case isoDate = "MMM, dd, YYYY"
        case iso8601 = "MMM dd, h:mm a"
        case date = "d/M/yy HH:mm"
        case japaneseDate = "yyyy年MM月dd日"
        case shortTime = "hh:mm a"

        var instance: DateFormatter {
            switch self {
            default:
                return DateFormatter().then {
                    $0.dateFormat = self.rawValue
                    $0.amSymbol = "AM"
                    $0.pmSymbol = "PM"
                    $0.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
                    $0.locale = Locale(identifier: "en_US_POSIX")
                }
            }
        }
    }
}

extension Date {
    func toDateString() -> String {
        let formatter = DateFormatter.Format.isoDate.instance
        return formatter.string(from: self)
    }

    func addedBy(minutes:Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }

    func addDay(day: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: day, to: self)!
    }
}
