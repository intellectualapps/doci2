//
//  RealmManager.swift
//  Education
//
//  Created by Nguyen Van Dung on 10/28/16.
//  Copyright © 2016 Dht. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

//Compatible app version. Example: 1.0 => 10, 1.2 -> 12
let schemaVersion = 1

typealias SaveDatabaseCompletion = (Bool) -> ()
class DatabaseManager: NSObject {
    
    static let shared = DatabaseManager()
    
    var realm: Realm!

    func write(_ constructor: () -> ()) {
        realm?.beginWrite()
    }

    override init() {
        super.init()
        prepare()
    }

    func prepare() {
        let destinatation = String.documentFolder() + "/data.realm"
        let url = URL(fileURLWithPath: destinatation)
        var config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: UInt64(schemaVersion),

            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < schemaVersion) {
//                    migration.enumerateObjects(ofType: PlayListEntity.className(), { (obj, obj2) in
//                        obj2?["isHidden"] = false
//                    })
//                    migration.enumerateObjects(ofType: SetEntity.className(), { (old, new) in
//                        new?["productId"] = ""
//                    })
                }
        })
        config.fileURL = url
        Realm.Configuration.defaultConfiguration = config
        realm = try? Realm()
    }

    func save(obj: Object, update: Bool = true, complete: SaveDatabaseCompletion) {
        realm?.beginWrite()
        self.realm?.add(obj, update: update)
        try? realm?.commitWrite()
        complete(true)
    }
}
