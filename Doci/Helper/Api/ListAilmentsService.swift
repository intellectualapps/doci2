//
//  ListAilmentsService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 8/1/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class ListAilmentsService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var results = [Ailments]()
        if let root = response as? [String: Any] {
            if let datas = root["data"] as? [[String: Any]] {
                datas.forEach { (d) in
                    let info = Ailments(dict: d)
                    results.append(info)
                }
            }
        }
        super.onFinish(results, error: error, completion: completion)
    }

    class func getListAliments(completion: @escaping NetworkServiceCompletion) {
        let path = Path.listAilments.path
        let params = RequestParams.defaultLoginParams()
        let service = ListAilmentsService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
    }
}
