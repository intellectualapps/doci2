//
//  ResetPasswordService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/26/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class VerifyOTPService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        super.onFinish(response, error: error, completion: completion)
    }

    class func resetPassword(user_id: String, otp_type: String, otp_code: String, completion: @escaping NetworkServiceCompletion) {
        let path = Path.verifyOTP.path
        let params = RequestParams()
        params.setValue(user_id, forKey: "user_id")
        params.setValue(otp_type, forKey: "otp_type")
        params.setValue(otp_code, forKey: "otp_code")
        let service = VerifyOTPService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        service.doExecute(completion)
    }
}
