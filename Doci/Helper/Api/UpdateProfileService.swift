//
//  UpdateProfileService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 9/8/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class UpdateProfileService: ApiServices {

    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        if let root = response as? [String: Any] {
            if let data = root["data"] as? [String: Any] {
                let user = User.userFromDict(dict: data, useAsLogin: false)
                var loggedIn = AppDelegate.shared.loggedUser.value
                loggedIn?.phone = user?.phone ?? ""
                loggedIn?.save()
                AppDelegate.shared.loggedUser.accept(loggedIn)
            }
        }
        super.onFinish(response, error: error, completion: completion)
    }

    class func updateProfile(user: User, completion: @escaping NetworkServiceCompletion) {
        let path = Path.updateProfile(id: user.id).path
        let params = RequestParams()
        params.setValue(user.phone, forKey: "phone")
        params.setValue(user.name, forKey: "name")
        let service = UpdateProfileService(apiPath: path, method: .put, requestParam: params, paramEncoding: Encoding.forMethod(method: .put), retryCount: 1)
        service.doExecute(completion)
    }
}
