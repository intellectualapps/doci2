//
//  ResetPasswordService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/26/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class SearchHCPService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var results = [Doctor]()
        if let root = response as? [String: Any] {
            if let datas = root["data"] as? [[String: Any]] {
                datas.forEach { (d) in
                    let info = Doctor(dict: d)
                    results.append(info)
                }
            }
        }
        super.onFinish(results, error: error, completion: completion)
    }

    class func searchHCP(search_type: Int, search_id: Int, offset: Int, completion: @escaping NetworkServiceCompletion) {
        let path = Path.searchHCP.path
        let params = RequestParams()
        params.setValue(search_type, forKey: "search_type")
        params.setValue(search_id, forKey: "search_id")
        params.setValue(offset, forKey: "offset")
        let service = SearchHCPService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
    }
}
