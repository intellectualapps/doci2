//
//  DisconnectFacebookService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 9/12/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class DisconnectFacebookService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        if let root = response as? [String: Any] {
            print(root)
        }
        super.onFinish(response, error: error, completion: completion)
    }

    class func disconnectFacebook(socialInfo: SocialInfo, completion: @escaping NetworkServiceCompletion) {
        let userid = String(AppDelegate.shared.loggedUser.value?.id ?? -1)
        let path = Path.connectFacebook(userid: userid).path
        let params = RequestParams()
        params.setValue(userid, forKey: "user_id")
        let service = DisconnectFacebookService(apiPath: path, method: .delete, requestParam: params, paramEncoding: Encoding.forMethod(method: .delete), retryCount: 1)
        service.doExecute(completion)
    }
}
