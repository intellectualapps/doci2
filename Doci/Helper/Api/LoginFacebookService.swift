//
//  LoginFacebookService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 9/4/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class LoginFacebookService: ApiServices {

    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var user: User?
        if let root = response as? [String: Any] {
            if let data = root["data"] as? [String: Any] {
                user = User.userFromDict(dict: data)
                user?.save()
                AppDelegate.shared.loggedUser.accept(user)
            }
        }
        super.onFinish(user, error: error, completion: completion)
    }

    class func loginFacebook(name: String, email: String, token: String, photo: String, photoSmall: String, completion: @escaping NetworkServiceCompletion) {
        let path = Path.loginFacebook.path
        let params = RequestParams()
        params.setValue(name, forKey: "name")
        params.setValue(email, forKey: "email")
        params.setValue(token, forKey: "access_token")
        params.setValue(photo, forKey: "photo_url_big")
        params.setValue(photoSmall, forKey: "photo_url_small")
        let service = LoginFacebookService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        service.doExecute(completion)
    }
}
