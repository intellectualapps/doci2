//
//  UploadPhotoService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 8/30/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

struct UploadPhotoCompleted {
    var message = ""
    var bigPhotoUrl = ""
    var smallPhotoUrl = ""
}

class UploadPhotoService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var result = UploadPhotoCompleted(message: "", bigPhotoUrl: "", smallPhotoUrl: "")
        if let root = response as? [String: Any] {
            if let data = root["data"] as? [String: Any] {
                result.bigPhotoUrl = data.stringOrEmptyForKey(key: "photo_url_big")
                result.smallPhotoUrl = data.stringOrEmptyForKey(key: "photo_url_small")
            }
            result.message = root.stringOrEmptyForKey(key: "message")
        }
        super.onFinish(result, error: error, completion: completion)
    }

    class func uploadPhoto(data: Data, completion: @escaping NetworkServiceCompletion) {
        let id = AppDelegate.shared.loggedUser.value?.id ?? -1
        let params = RequestParams()
        let body = RequestFileBodyData(name: "profile", data: data, mintype: "image/png", filename: "avatar.png")
        params.addBodyData(bodyData: body)
        let path = Path.uploadPhoto(id: id).path
        let service = UploadPhotoService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        service.doExecute(completion)
    }
}
