//
//  ListSpecifiesService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 8/1/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class ListSpecifiesService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        var results = [Specialties]()
        if let root = response as? [String: Any] {
            if let datas = root["data"] as? [[String: Any]] {
                datas.forEach { (d) in
                    let info = Specialties(dict: d)
                    results.append(info)
                }
            }
        }
        super.onFinish(results, error: error, completion: completion)
    }

    class func getListSpecifies(completion: @escaping NetworkServiceCompletion) {
        let path = Path.listSpecialties.path
        let params = RequestParams.defaultLoginParams()
        let service = ListSpecifiesService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
    }
}
