//
//  CreateBookHCPService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 9/12/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class CreateBookHCPService: ApiServices {

    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        if let root = response as? [String: Any] {
            print(root)
        }
        super.onFinish(response, error: error, completion: completion)
    }

    class func createBookHCP(doctor: Doctor, completion: @escaping NetworkServiceCompletion) {
        let path = Path.bookings.path
        let params = RequestParams()
        params.setValue(Date().timeIntervalSince1970, forKey: "timestamp")
        params.setValue(doctor.service, forKey: "services")
        params.setValue("", forKey: "condition")
        params.setValue(0, forKey: "total_cost")
        params.setValue(doctor.id, forKey: "hcp_id")
        params.setValue(doctor.id, forKey: "hcp_user_id")
        params.setValue(doctor.rate, forKey: "hcp_rating")
        params.setValue(doctor.avartUrl, forKey: "hcp_photo_url")
        params.setValue(doctor.name, forKey: "hcp_name")
        params.setValue(doctor.specialty, forKey: "hcp_specialty")
        params.setValue(doctor.service, forKey: "hcp_services")
        params.setValue(doctor.schedule, forKey: "hcp_schedule")
        params.setValue(doctor.name, forKey: "user_name")
        params.setValue(doctor.avartUrl, forKey: "user_photo_url")
        params.setValue(doctor.allowsToViewHistory, forKey: "allow_to_view_history")

        let service = CreateBookHCPService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        service.doExecute(completion)
    }
}
