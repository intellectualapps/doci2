//
//  ResetPasswordService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/26/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class GenerateOTPService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        super.onFinish(response, error: error, completion: completion)
    }

    class func resetPassword(user_id: String, otp_type: String, email: String, code: String, completion: @escaping NetworkServiceCompletion) {
        let path = Path.generateOTP.path
        let params = RequestParams()
        params.setValue(email, forKey: "email")
        params.setValue(user_id, forKey: "user_id")
        params.setValue(otp_type, forKey: "otp_type")
        params.setValue(code, forKey: "code")
        let service = GenerateOTPService(apiPath: path, method: .get, requestParam: params, paramEncoding: Encoding.forMethod(method: .get), retryCount: 1)
        service.doExecute(completion)
    }
}
