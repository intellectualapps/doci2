//
//  SearchMapActor.swift
//  kids_taxi
//
//  Created by Nguyen Van Dung on 6/27/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

import Foundation
import Alamofire
import GoogleMaps
import GooglePlaces

class SearchMapActor: DhtActor {
    var service: ApiServices?
    
    override class func genericPath()-> String? {
        return "/SearchMapQueue/@";
    }
    
    class func executePath() -> String {
        return "/SearchMapQueue/(fetch)";
    }
    
    class func executeSearchMapAddress(_ address: String, watcher: DhtWatcher) {
        let path = SearchMapActor.executePath()
        //cancel
        actionControlInstance().remove(watcher, fromPath: path)
        DhtActor.registerClass(SearchMapActor.self)
        var options = [AnyHashable: Any]()
        options["address"] = address
        actionControlInstance().dispatch {
            actionControlInstance().requestActor(path, options: options, flags: PriorityFlags.ActionRequestNormal, watcher: watcher)
        }
    }
    
    override func cancel() {
        self.service?.cancel()
    }
    override func execute(_ options: [AnyHashable: Any]!) {
        if let address = options["address"] as? String {
            let filter = GMSAutocompleteFilter()
            filter.country = "VN"
            let placesClient = GMSPlacesClient()
            placesClient.autocompleteQuery(address, bounds: GMSCoordinateBounds(), filter: filter, callback: { (results, error) in
                guard error == nil else {
                    actionControlInstance().actionCompleted(self.path, result: [], error: nil)
                    return
                }

                var listLocations = [VLocation]()
                for result in results! {
                    let location = VLocation()
                    var address = ""
                    if let range = result.attributedFullText.string.range(of: result.attributedPrimaryText.string), !range.isEmpty {
                        address = result.attributedFullText.string
                    }
                    location.formatedAddress = address
                    location.name = result.attributedPrimaryText.string
                    location.calculateHeight(maxWidth: 0)
                    listLocations.append(location)
                    print(address)
                }
                Utils.dispatch(onMainAsyncSafe: {
                    actionControlInstance().actionCompleted(self.path, result: listLocations, error: nil)
                })
            })
        }
    }
}
