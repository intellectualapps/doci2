//
//  ConnectFacebookService.swift
//  Doci
//
//  Created by Nguyen Van Dung on 9/12/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class ConnectFacebookService: ApiServices {
    override func onFinish(_ response: Any?, statusCode: Int = 0, error: ErrorInfo?, completion: NetworkServiceCompletion?) {
        if let root = response as? [String: Any] {
            print(root)
        }
        super.onFinish(response, error: error, completion: completion)
    }

    class func connectFacebook(socialInfo: SocialInfo, completion: @escaping NetworkServiceCompletion) {

        let userid = String(AppDelegate.shared.loggedUser.value?.id ?? -1)
        let path = Path.connectFacebook(userid: userid).path
        let params = RequestParams()
        params.setValue(socialInfo.name, forKey: "name")
        params.setValue(socialInfo.email, forKey: "email")
        params.setValue(socialInfo.token, forKey: "access_token")
        params.setValue(socialInfo.photo, forKey: "photo_url_big")
        params.setValue(socialInfo.photo, forKey: "photo_url_small")
        params.setValue(socialInfo.photo.count > 0, forKey: "has_photo")
        let service = ConnectFacebookService(apiPath: path, method: .post, requestParam: params, paramEncoding: Encoding.forMethod(method: .post), retryCount: 1)
        service.doExecute(completion)
    }
}
