//
//  DhtActivityView.m
//  Fonext
//
//  Created by dungnv9 on 3/17/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtActivityView.h"
@interface DhtActivityView()
{
    UIActivityIndicatorView     *_indicatorView;
}

@end
@implementation DhtActivityView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        _indicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _indicatorView.hidesWhenStopped = false;
        _indicatorView.tintColor = [UIColor blueColor];
        [self addSubview:_indicatorView];
        [_indicatorView stopAnimating];
    }
    return self;
}

- (void) setAlphaForViews:(CGFloat)alpha {
    self.alpha = alpha;
    if (alpha ==0) {
        [_indicatorView removeFromSuperview];
    } else {
        [self addSubview:_indicatorView];
    }
}

- (void) startAcitivty {
    [_indicatorView startAnimating];
}

- (void) stopActivity {
    [_indicatorView stopAnimating];
}

- (void) willBecomeRecycled {
    
}

- (void) layoutSubviews {
    [super layoutSubviews];
    CGRect r = _indicatorView.frame;
    r.origin.x = -5;
    r.origin.y = 0;
    _indicatorView.frame = r;
}
@end
