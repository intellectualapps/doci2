//
//  MenuContainer.h
//  MtoM
//
//  Created by Nguyen Van Dung on 3/31/16.
//  Copyright © 2016 Framgia. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MenuView;
@interface MenuContainer : UIView
@property (nonatomic, strong) MenuView *menuView;

@property (nonatomic, readonly) bool isShowingMenu;
@property (nonatomic) CGRect showingMenuFromRect;

- (void)showMenuFromRect:(CGRect)rect;
- (void)hideMenu;
@end
