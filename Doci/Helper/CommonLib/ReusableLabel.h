//
//  ReusableLabel.h
//  Kara_iOS
//
//  Created by nguyen van dung on 6/4/15.
//  Copyright (c) 2015 Dht. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Utils.h"
#import <CoreText/CoreText.h>
#define UIColorRGB(rgb) ([[UIColor alloc] initWithRed:(((rgb >> 16) & 0xff) / 255.0f) green:(((rgb >> 8) & 0xff) / 255.0f) blue:(((rgb) & 0xff) / 255.0f) alpha:1.0f])
#define UIColorRGBA(rgb,a) ([[UIColor alloc] initWithRed:(((rgb >> 16) & 0xff) / 255.0f) green:(((rgb >> 8) & 0xff) / 255.0f) blue:(((rgb) & 0xff) / 255.0f) alpha:a])
#ifdef __cplusplus
#include <vector>

#endif

#ifdef __cplusplus

typedef struct
{
    float offset;
    float horizontalOffset;
    uint8_t alignment;
    CGFloat lineWidth;
} LinePosition;

class LinkData
{
public:
    NSRange range;
    NSString *url;
    
    CGRect topRegion;
    CGRect middleRegion;
    CGRect bottomRegion;
    
public:
    LinkData(NSRange range_, NSString *url_)
    {
        range = range_;
        url = url_;
        
        topRegion = CGRectZero;
        middleRegion = CGRectZero;
        bottomRegion = CGRectZero;
    }
    
    LinkData(const LinkData &other)
    {
        range = other.range;
        url = other.url;
        
        topRegion = other.topRegion;
        middleRegion = other.middleRegion;
        bottomRegion = other.bottomRegion;
    }
    
    LinkData & operator= (const LinkData &other)
    {
        if (this != &other)
        {
            range = other.range;
            url = other.url;
            
            topRegion = other.topRegion;
            middleRegion = other.middleRegion;
            bottomRegion = other.bottomRegion;
        }
        
        return *this;
    }
    
    ~LinkData()
    {
        url = nil;
    }
};

#endif

@interface ReusableLabelLayoutData : NSObject

@property (nonatomic) CGSize size;
@property (nonatomic, assign) int numberOfLines;

- (NSString *)linkAtPoint:(CGPoint)point topRegion:(CGRect *)topRegion middleRegion:(CGRect *)middleRegion bottomRegion:(CGRect *)bottomRegion;


@end
@interface ReusableLabel : UIView
@property (nonatomic, strong) NSString *reuseIdentifier;

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSAttributedString *attributedText;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIColor *highlightedTextColor;
@property (nonatomic, strong) UIColor *shadowColor;
@property (nonatomic, strong) UIColor *highlightedShadowColor;
@property (nonatomic) bool richText;
@property (nonatomic) CGSize shadowOffset;
@property (nonatomic) bool highlighted;
@property (nonatomic) int numberOfLines;
@property (nonatomic) NSTextAlignment textAlignment;

+ (void)preloadData;
@property (nonatomic, retain) ReusableLabelLayoutData *precalculatedLayout;;
+ (ReusableLabelLayoutData *)calculateLayout:(NSString *)text additionalAttributes:(NSArray *)additionalAttributes textCheckingResults:(NSArray *)textCheckingResults font:(CTFontRef)font textColor:(UIColor *)textColor frame:(CGRect)frame orMaxWidth:(float)maxWidth flags:(int)flags textAlignment:(NSTextAlignment)textAlignment outIsRTL:(bool *)outIsRTL maxLine:(NSInteger)maxLine;

+ (ReusableLabelLayoutData *)calculateLayout:(NSString *)text
                        additionalAttributes:(NSArray *)additionalAttributes
                         textCheckingResults:(NSArray *)textCheckingResults
                                        font:(CTFontRef)font
                                   textColor:(UIColor *)textColor
                                   linkColor:(UIColor *)linkColor
                                       frame:(CGRect)frame
                                  orMaxWidth:(float)maxWidth
                                       flags:(int)flags
                               textAlignment:(NSTextAlignment)textAlignment
                                    outIsRTL:(bool *)outIsRTL
                     additionalTrailingWidth:(CGFloat)additionalTrailingWidth
                                     maxLine:(NSInteger)maxLine;

+ (void)drawTextInRect:(CGRect)rect
                  text:(NSString *)text
              richText:(bool)richText
                  font:(UIFont *)font
           highlighted:(bool)highlighted
             textColor:(UIColor *)textColor
      highlightedColor:(UIColor *)highlightedColor
           shadowColor:(UIColor *)shadowColor
          shadowOffset:(CGSize)shadowOffset
         numberOfLines:(int)numberOfLines;

+ (void)drawRichTextInRect:(CGRect)rect precalculatedLayout:(ReusableLabelLayoutData *)precalculatedLayout linesRange:(NSRange)linesRange shadowColor:(UIColor *)shadowColor shadowOffset:(CGSize)shadowOffset needTruncattailLastLine:(BOOL)truncate maxWidthCompare:(CGFloat) width;
@end
