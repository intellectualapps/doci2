//
//  ChatController.h
//  kids_taxi
//
//  Created by Nguyen Van Dung on 5/7/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatController : UIViewController
@property (nonatomic, strong, nonnull) NSMutableArray *items;
+ (void)dispatchOnMessageQueue:(_Nonnull dispatch_block_t)block;
+ (bool)isMessageQueue;
@end
