//
//  DhtLocationTracker.m
//  KidTaxiLib
//
//  Created by Nguyen Van Dung on 9/13/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "DhtLocationTracker.h"
#import "KAlertViewController.h"
#import "Utils.h"
#define LATITUDE @"latitude"
#define LONGITUDE @"longitude"
#define ACCURACY @"theAccuracy"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface DhtLocationTracker()
@property (nonatomic, assign) BOOL isTracking;
@property (nonatomic, strong) NSTimer *timeout;
@end
@implementation DhtLocationTracker
+ (CLLocationManager *)sharedLocationManager {
    static CLLocationManager *_locationManager;
    @synchronized(self) {
        if (_locationManager == nil) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
            _locationManager.pausesLocationUpdatesAutomatically = NO;
        }
    }
    return _locationManager;
}

- (id)init {
    self  = [super init];
    if (self) {
        //Get the share model and also initialize myLocationArray
        self.shareModel = [DhtLocationModel shareModel];
        self.shareModel.myLocationArray = [[NSMutableArray alloc]init];
        [self restartLocationUpdates];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)applicationEnterBackground {
    [self restartLocationUpdates];
    //Use the BackgroundTaskManager to manage all the background Task
    self.shareModel.bgTask = [DhtBackgroundTaskManager sharedBackgroundTaskManager];
    [self.shareModel.bgTask beginNewBackgroundTask];
}

- (void) restartLocationUpdates {
    if (self.shareModel.timer) {
        [self.shareModel.timer invalidate];
        self.shareModel.timer = nil;
    }
    CLLocationManager *locationManager = [DhtLocationTracker sharedLocationManager];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager requestWhenInUseAuthorization];
}

- (void)startTracking {
    CLLocationManager *locationManager = [DhtLocationTracker sharedLocationManager];
    [locationManager startUpdatingLocation];
    [locationManager startUpdatingHeading];
}

- (void)startTimeOut {
    [self stopTimeOut];
}

- (void)stopTimeOut {
    _timeout = [NSTimer scheduledTimerWithTimeInterval: 20 target: self selector: @selector(executeFetchLocationTimeout) userInfo: nil repeats: NO];
}

- (void)executeFetchLocationTimeout {
    [self stopTimeOut];
    [self didGetLocation: [self bestLocation]];
}

- (void)startLocationTracking {
    [self startTimeOut];
    if (!self.isTracking) {
        self.isTracking = true;
        //This valid is true if app running and not in force ground mode.
        BOOL appDidResign = [[NSUserDefaults standardUserDefaults] boolForKey: @"appResignActive"];
        if (appDidResign) {
            [self processShowAlertLocationPermissionIfNeed: [CLLocationManager authorizationStatus]];
        }
    }
}

- (void)processShowAlertLocationPermissionIfNeed:(CLAuthorizationStatus)status {
    BOOL needShowPermissionAlert = NO;
    if ([CLLocationManager locationServicesEnabled] == NO) {
        needShowPermissionAlert = YES;
    } else {
        switch (status) {
            case kCLAuthorizationStatusNotDetermined:
                [self restartLocationUpdates];
                break;
            case kCLAuthorizationStatusAuthorizedWhenInUse:
            case kCLAuthorizationStatusAuthorizedAlways:
                [self startTracking];
                break;
            case kCLAuthorizationStatusRestricted:
                needShowPermissionAlert = YES;
                break;
            case kCLAuthorizationStatusDenied:
                needShowPermissionAlert = YES;
                break;
            default:
                break;
        }
    }

    if (needShowPermissionAlert) {
        [self didGetLocation: nil];
        [self closeOtherAlertIfNeed: NO];
        UIViewController *rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
        UIViewController *parentController = [KAlertViewController findTopMostPresentedController: rootController];
        if (parentController == nil) {
          parentController = rootController;
        }
        //Do not show again if already show before
        if (parentController && [parentController isKindOfClass: [KAlertViewController class]] ) {
          return;
        }

        KAlertViewController *alertController = [KAlertViewController alertControllerWithTitle: @""
                                                                                       message:NSLocalizedString(@"alert_please_allow_use_of_location_information", "")
                                                                                preferredStyle:UIAlertControllerStyleAlert];
        NSString *okTitle = NSLocalizedString(@"btn_goto_setting", "");
        UIAlertAction *okAction = [UIAlertAction actionWithTitle: okTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationWillGotoSettingAppFromLocationPermission object: nil];
            NSURL *url = [NSURL URLWithString: UIApplicationOpenSettingsURLString];
            if (url) {
                [[UIApplication sharedApplication] openURL: url];
            }
        }];
        [alertController addAction: okAction];


        [parentController presentViewController: alertController animated: YES completion:NULL];
    } else {
        [self closeOtherAlertIfNeed: YES];
    }
}

- (void)closeOtherAlertIfNeed:(BOOL)isForceClose {
    UIViewController *rootController = [KAlertViewController findTopMostPresentedController: nil];
    // Do not show permiss alert more time
    if (rootController) {
        if ([rootController isKindOfClass: [KAlertViewController class]] && !isForceClose) {
            return;
        }
        if ([rootController isKindOfClass: [UIAlertController class]]) {
            [rootController dismissViewControllerAnimated: false completion: NULL];
        }
    }
}

- (void)stopLocationTracking {
    self.isTracking = false;
    if (self.shareModel.timer) {
        [self.shareModel.timer invalidate];
        self.shareModel.timer = nil;
    }
    CLLocationManager *locationManager = [DhtLocationTracker sharedLocationManager];
    [locationManager stopUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate Methods

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    for (int i = 0; i < locations.count; i++) {
        CLLocation * newLocation = [locations objectAtIndex:i];
        CLLocationCoordinate2D theLocation = newLocation.coordinate;
        CLLocationAccuracy theAccuracy = newLocation.horizontalAccuracy;
        //Select only valid location and also location with good accuracy
        if(newLocation != nil &&
           (!(theLocation.latitude == 0.0 &&
              theLocation.longitude == 0.0))) {
            self.myLastLocation = theLocation;
            self.myLastLocationAccuracy= theAccuracy;
            //Add the vallid location with good accuracy into an array
            //Every 1 minute, I will select the best location based on accuracy and send to server
            //only hold max 50 object
            if (self.shareModel.myLocationArray.count > 50) {
                for (NSInteger i = self.shareModel.myLocationArray.count - 1; i >= 0; i--) {
                    if (i > 50) {
                        [self.shareModel.myLocationArray removeObjectAtIndex:i];
                    }
                }
            }
            [self didGetLocation: newLocation];
            [self.shareModel.myLocationArray addObject: [newLocation copy]];
            [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationDidUpdateLocation object: newLocation];
        }
    }
    CLLocation *location = [self bestLocation];
    if (location) {
        [self didGetLocation: location];
    }
    //If the timer still valid, return it (Will not run the code below)
    if (self.shareModel.timer) {
        return;
    }

    self.shareModel.bgTask = [DhtBackgroundTaskManager sharedBackgroundTaskManager];
    [self.shareModel.bgTask beginNewBackgroundTask];

    //Restart the locationMaanger after 1 minute
    self.shareModel.timer = [NSTimer scheduledTimerWithTimeInterval:60 target:self
                                                           selector:@selector(restartLocationUpdates)
                                                           userInfo:nil
                                                            repeats:NO];

    //Will only stop the locationManager after 10 seconds, so that we can get some accurate locations
    //The location manager will only operate for 10 seconds to save battery
    if (self.shareModel.delay10Seconds) {
        [self.shareModel.delay10Seconds invalidate];
        self.shareModel.delay10Seconds = nil;
    }

    self.shareModel.delay10Seconds = [NSTimer scheduledTimerWithTimeInterval:10 target:self
                                                                    selector:@selector(stopLocationDelayBy10Seconds)
                                                                    userInfo:nil
                                                                     repeats:NO];

}

- (void)locationManager:(CLLocationManager *)manager
       didUpdateHeading:(CLHeading *)newHeading{
    [[NSNotificationCenter defaultCenter] postNotificationName: KNotificationDidUpdateLocationHeading object: newHeading];
}



//Stop the locationManager
-(void)stopLocationDelayBy10Seconds {
    //    CLLocationManager *locationManager = [DhtLocationTracker sharedLocationManager];
    //    [locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    [self processShowAlertLocationPermissionIfNeed: status];
}

- (void)locationManager: (CLLocationManager *)manager didFailWithError: (NSError *)error {
    CLLocation *location = [self bestLocation];
    [self didGetLocation: location];
}

- (void)didGetLocation: (CLLocation *)location {
    if (self.completionHandler) {
        self.completionHandler(location, nil);
        self.completionHandler = nil;
    }
}

- (CLLocation *)bestLocation {
    CLLocation * myBestLocation = [self.shareModel.myLocationArray lastObject];
    return myBestLocation;
}

- (CLLocationCoordinate2D)bestCurrentLocationCoordinate {
    CLLocation * myBestLocation = nil;
    for (int i = 0; i < self.shareModel.myLocationArray.count; i++) {
        CLLocation* currentLocation = [self.shareModel.myLocationArray objectAtIndex:i];
        if (i == 0) {
            myBestLocation = currentLocation;
        } else {
            if(currentLocation.horizontalAccuracy <= myBestLocation.horizontalAccuracy) {
                myBestLocation = currentLocation;
            }
        }
    }
    if (self.shareModel.myLocationArray.count == 0) {
        self.myLocation = self.myLastLocation;
        self.myLocationAccuracy = self.myLastLocationAccuracy;

    } else {
        self.myLocation = myBestLocation.coordinate;
        self.myLocationAccuracy = myBestLocation.horizontalAccuracy;
    }
    return self.myLocation;
}

- (void)clear {
    [self.shareModel.myLocationArray removeAllObjects];
    self.shareModel.myLocationArray = nil;
    self.shareModel.myLocationArray = [[NSMutableArray alloc]init];
}
@end
