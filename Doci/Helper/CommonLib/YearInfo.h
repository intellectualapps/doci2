//
//  YearInfo.h
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MonthInfo;
@interface YearInfo : NSObject
- (void)addMonth:(MonthInfo *)info;
@end
