//
//  DhtNotificationViewController.h
//  Fonext
//
//  Created by nguyen van dung on 4/23/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DhtNotificationView;
@interface DhtNotificationViewController : UIViewController
{
    
}
@property (nonatomic, strong) DhtNotificationView       *notificationView;
@property (nonatomic, weak) UIWindow    *weakWindow;
@end
