//
//  DhtCalendarContentView.h
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DhtCalendarUtils.h"
@class MonthInfo;
@class DhtCalendarDayCell;

@interface DhtCalendarContentView : UIView
@property (nonatomic, strong) MonthInfo *monthInfo;
@property (nonatomic, weak) id<DhtCalendarViewDelegate> delegate;
- (void)reloadContent:(MonthInfo *)mInfo;
- (CGSize)sizeForDayView;
- (void)draw;
- (CGFloat)totalHeight;
- (void)log;
- (NSDate *)selectedDay;
- (NSArray *)dayViews;
- (void)resetSelections;
- (NSInteger)numberOfDaysSelected;
- (void)applyMode:(BOOL)isCreateNewCalendar;
- (void)renderView;
- (DhtCalendarDayCell *)dayViewWithDate:(NSDate *)date;
@end
