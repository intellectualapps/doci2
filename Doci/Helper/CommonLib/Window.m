//
//  Window.m
//  FTaxiLib
//
//  Created by Nguyen Van Dung on 4/3/17.
//  Copyright © 2017 Dht. All rights reserved.
//

#import "Window.h"

@implementation Window
+ (UIWindow *)keyWindow {
    return [UIApplication sharedApplication].keyWindow;
}
@end
