//
//  DhtTextView.m
//  ChatPro
//
//  Created by dungnv9 on 2/25/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtTextView.h"

@interface DhtTextView()
@property (nonatomic, strong) NSString *viewIdentifier;
@property (nonatomic, strong) NSString *viewStateIdentifier;
@end
@implementation DhtTextView
@synthesize viewIdentifier = _viewIdentifier;
@synthesize viewStateIdentifier = _viewStateIdentifier;

- (id)init {
    self = [super init];
    if (self) {
        _label = [[UILabel alloc] initWithFrame:CGRectZero];
        _label.textAlignment = NSTextAlignmentLeft;
        _label.textColor = [UIColor blackColor];
        [self addSubview:_label];
    }
    return self;
}

- (void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    self.label.frame = self.bounds;
}

- (void)willBecomeRecycled {
}

@end
