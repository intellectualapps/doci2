//
//  DhtMessageNotificationView.m
//  Fonext
//
//  Created by nguyen van dung on 4/23/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtMessageNotificationView.h"
#import "DhtMessageNotificationLabel.h"
#import "DhtNotificationWindow.h"
#import "Utils.h"
#import "DhtButtonView.h"

@interface DhtMessageNotificationView()
{
    
}

@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) CALayer *shadowLayer;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) DhtMessageNotificationLabel *messageLabel;
@property (nonatomic, strong) DhtButtonView *dismissButton;

@end
@implementation DhtMessageNotificationView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 64)];
        _backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _backgroundView.backgroundColor = UIColorRGBA(0x000000, 0.8f);
        [self addSubview:_backgroundView];
        
        _shadowLayer = [[CALayer alloc] init];
        _shadowLayer.backgroundColor = UIColorRGBA(0x000000, 0.5f).CGColor;
        _shadowLayer.frame = CGRectMake(0, 63, self.bounds.size.width, 1);
        [self.layer addSublayer:_shadowLayer];
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(64, 3, self.bounds.size.width - 8 - 32 - 70, 18)];
        _titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = MediumSystemFontOfSize(16);
        _titleLabel.numberOfLines = 1;
        _titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [self addSubview:_titleLabel];
        
        
        UIImage *buttonImage = [UIImage imageNamed:@"icon_delete"];
        _dismissButton = [[DhtButtonView alloc] initWithFrame:CGRectMake(self.bounds.size.width - buttonImage.size.width - 10, 23, buttonImage.size.width, buttonImage.size.height)];
        _dismissButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        _dismissButton.exclusiveTouch = true;
        [_dismissButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
        _dismissButton.extendedEdges = UIEdgeInsetsMake(20, 20, 20, 20);
        [self addSubview:_dismissButton];
        
        [_dismissButton addTarget:self action:@selector(dismissButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        _messageLabel = [[DhtMessageNotificationLabel alloc] initWithFrame:CGRectMake(10, 23, _dismissButton.frame.origin.x - 10, 36)];
        _messageLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _messageLabel.backgroundColor = [UIColor clearColor];
        _messageLabel.textColor = [UIColor whiteColor];
        _messageLabel.font = SystemFontOfSize(14);
        _messageLabel.numberOfLines = 2;
        _messageLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [self addSubview:_messageLabel];
        
        _titleLabel.userInteractionEnabled = false;
        _messageLabel.userInteractionEnabled = false;
        
        _backgroundView.userInteractionEnabled = true;
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognized:)];
        [self addGestureRecognizer:tapRecognizer];
    }
    return self;
}

- (void) setMessageInfo:(id<NotificationProtocol>)object {
    self.object = object;
    _titleLabel.text = [object title];
    _messageLabel.text = [object notifyMessage];
}

- (void)searchParentAndDismiss:(UIView *)view {
    if (view == nil)
        return;
    
    if ([view isKindOfClass:[DhtNotificationWindow class]]) {
        [((DhtNotificationWindow *)view) animateOut];
    } else {
        [self searchParentAndDismiss:view.superview];
    }
}

- (void)searchParentAndTap:(UIView *)view {
    if (view == nil) {
        return;
    }

    if ([view isKindOfClass:[DhtNotificationWindow class]]) {
        [((DhtNotificationWindow *)view) performTapAction:self.object];
    } else {
        [self searchParentAndTap:view.superview];
    }
}

- (void)dismissButtonPressed {
    [self searchParentAndDismiss:self.superview];
}

- (void) openMesssage {
    [self searchParentAndTap:self.superview];
}

- (void)tapGestureRecognized:(UITapGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateRecognized) {
        [self searchParentAndTap:self.superview];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _shadowLayer.frame = CGRectMake(0, 63, self.bounds.size.width, 1);
}
@end
