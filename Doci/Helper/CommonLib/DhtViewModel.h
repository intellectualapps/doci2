//
//  DhtViewModel.h
// Kids_taxi
//
//  Created by nguyenvandung on 9/29/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseView.h"
#import "DhtViewContext.h"
#import "DhtViewStorage.h"
#import "DhtViewModel.h"

@class DhtViewStorage;
@class MessageClone;
@class GenericInfo;

@interface DhtViewModel : NSObject
{
@public
    struct {
        int hasNoView : 1;
        int skipDrawInContext : 1;
        int disableSubmodelAutomaticBinding : 1;
        int viewUserInteractionDisabled : 1;
    } _modelFlags;
}
@property (nonatomic) CGRect frame;
@property (nonatomic) float alpha;
@property (nonatomic) bool hidden;
@property (nonatomic, strong) NSString *viewStateIdentifier;
@property (nonatomic, strong) DhtViewContext  *viewContext;
@property (nonatomic) bool needsRelativeBoundsUpdates;
- (Class)viewClass;
- (UIView<BaseViewProtocol> *)_dequeueView:(DhtViewStorage *)viewStorage;
- (UIView<BaseViewProtocol> *)boundView;
- (void) bindViewToContainer:(UIView *)container viewStorage:(DhtViewStorage *)viewStorage;
- (void) unbindView:(DhtViewStorage *)viewStorage;
- (void) addSubmodel:(DhtViewModel *)model;
- (void) removeSubmodel:(DhtViewModel *)model viewStorage:(DhtViewStorage *)viewStorage;
- (void)moveViewToContainer:(UIView *)container;
- (void) layoutForContainerSize:(CGSize) containerSize;
- (void) setHasNoView;
- (void) setSkipDrawInContext:(BOOL)skip;
- (bool) skipDrawInContext;
- (void) updateMediaAvaibility;
- (CGRect)bounds;
- (void) drawInContext:(CGContextRef)context;
- (void) drawSubmodelsInContext:(CGContextRef)context;
- (void)relativeBoundsUpdated:(CGRect)bounds;
- (void) setUserInteraction:(BOOL)userInteraction;
- (void) updateReportStatus:(BOOL)isReported;
- (void) updateWithNewestContent:(id)content;
- (void) updateEventTagText:(NSString *)tag;
- (void) updateCommentCountInfo:(NSString *)pointText viewStorage:(DhtViewStorage *)viewStorage;
- (void) updateFirstItem:(id)model viewStorage:(DhtViewStorage *)storage;
- (void)updateMessage:(MessageClone *)msg viewStorage:(DhtViewStorage *)viewContext;
- (CGRect)effectiveContentFrame;
- (void)setTemporaryHighlighted:(bool)__unused temporaryHighlighted viewStorage:(DhtViewStorage *)__unused viewStorage;
- (void)updateItemInfo:(GenericInfo *)msg viewStorage:(DhtViewStorage *)viewStorage;
@end
