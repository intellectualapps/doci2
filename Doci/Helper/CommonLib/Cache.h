//
//  Cache.h
//  Cosplay No.1
//
//  Created by nguyenvandung on 10/7/15.
//  Copyright © 2015 Framgia. All rights reserved.
//
#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, CacheLocation) {
    CacheMemory = 1,
    CacheDisk = 2,
    CacheBoth = 1 | 2
} ;
#import <Foundation/Foundation.h>
#import "DhtUtility.h"

#ifdef __cplusplus
extern "C" {
#endif
    typedef enum {
        ScaleImageFlipVerical = 1,
        ScaleImageScaleOverlay = 2,
        ScaleImageRoundCornersByOuterBounds = 4,
        ScaleImageScaleSharper = 8
    } ScaleImageFlags;
UIImage *__ScaleImage(UIImage *image, CGSize size);
UIImage *__ScaleAndRoundCornersWithOffsetAndFlags(UIImage *image, CGSize size, CGPoint offset, CGSize imageSize, int radius, UIImage *overlay, bool opaque, UIColor *backgroundColor, int flags);
#ifdef __cplusplus
}
#endif
@interface Cache : NSObject

- (void)cacheImage:(UIImage *)image withData:(NSData *)data url:(NSString *)url cacheLocation:(NSInteger)location;
- (UIImage *)cachedImage:(NSString *)url cacheLocation:(NSInteger)location ;
- (UIImage *)cacheThumbnail:(UIImage *)image url:(NSString *)url cacheSizeType:(CGSize)size;
- (UIImage *)cachedThumbnail:(NSString *)url;
- (void) clearMemoryCache;
- (void) clearDiskCache;
- (void) clearThumbCache;
- (void) clearCacheForUrl:(NSString *)url;
- (void)removeFromMemoryCache:(NSString *)url matchEnd:(bool)matchEnd;
- (void)removeFromDiskCache:(NSString *)url;
- (void)diskCacheContains:(NSString *)url1 completion:(void (^)(bool cached))completion;
+ (Cache *) shareInstance;
@end
