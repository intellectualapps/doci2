//
//  MessageTextViewModel.h
//  MtoM
//
//  Created by nguyen van dung on 12/15/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "DhtViewModel.h"
#import "MessageTextBackgroundViewModel.h"
#import "DhtTextViewModel.h"

@class MessageClone;

@interface MessageTextViewModel : DhtViewModel
@property (nonatomic, strong) NSString    *localIdentifier;
@property (nonatomic, strong) MessageTextBackgroundViewModel* backgroundViewModel;
@property (nonatomic, strong) DhtTextViewModel  *textViewModel;
- (instancetype) initWithMessage:(MessageClone *)msg context:(DhtViewContext *)context;
- (void)setupBackground;
@end
