//
//  DhtHandle.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 1/31/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DhtWatcher.h"

@interface DhtHandle : NSObject
@property (nonatomic, weak) id<DhtWatcher> delegate;
@property (nonatomic) bool willReleaseOnMainThread;

- (bool)hasDelegate;
- (id)initWithDelegate:(id<DhtWatcher>)delegate willReleaseOnMainThread:(bool)willReleaseOnMainThread;
- (void)reset;
- (void)handleNotifyResourceDispatched:(NSString *)path resource:(id)resource arguments:(id)arguments;
- (void)receiveActorMessage:(NSString *)path messageType:(NSString *)messageType message:(id)message;
- (void)requestAction:(NSString *)action options:(id)options;
@end
