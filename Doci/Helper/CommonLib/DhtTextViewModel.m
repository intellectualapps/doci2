//
//  DhtTextViewModel.m
//  ChatPro
//
//  Created by dungnv9 on 2/25/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtTextViewModel.h"
#import "ReusableLabel.h"
#import "DhtTextView.h"
#import "Utils.h"

@interface DhtTextViewModel()
{
    ReusableLabelLayoutData *_layoutData;
    CGFloat _cachedLayoutContainerWidth;
}
@end
@implementation DhtTextViewModel

- (instancetype)initWithText:(NSString *)text
                 layoutFlags:(int)flag
                        font:(CTFontRef)font
                   textColor:(UIColor *)color
                     maxLine:(NSInteger)maxline
            willTruncateTail:(BOOL) willTruncateTail
{
    self = [super init];
    if (self != nil)
    {
        _lineCount = maxline;
        _layoutFlags = flag;
        _needTruncateLastLine = willTruncateTail;
        _additionalTrailingWidth = 10;
        self.alpha =1;
        self.textColor = [UIColor blueColor];
        if (text.length != 0)
            _text = text;
        else
            _text = @" ";
        
        if (font != NULL)
            _font = CFRetain(font);
        _textColor = color;
    }
    return self;
}

- (void)dealloc
{
    if (_font != NULL)
    {
        CFRelease(_font);
        _font = NULL;
    }
}

- (Class) viewClass
{
    return [DhtTextView class];
}

- (void)sizeToFit
{
    
}

- (void) bindViewToContainer:(UIView *)container viewStorage:(DhtViewStorage *)viewStorage
{
    [super bindViewToContainer:container viewStorage:viewStorage];
    DhtTextView *view = (DhtTextView *)[self boundView];
    if (view)
    {
        view.frame = self.frame;
        view.label.textColor = _textColor;
        view.label.text = (_text)?_text:@"";
        view.label.font = (__bridge UIFont * _Nullable)(_font);
        [container addSubview:view];
    }
}

- (void)drawInContext:(CGContextRef)context
{
    [super drawInContext:context];

    if (_layoutData != nil)
    {
        [ReusableLabel drawRichTextInRect:self.bounds
                      precalculatedLayout:_layoutData
                               linesRange:NSMakeRange(0, 0)
                              shadowColor:nil
                             shadowOffset:CGSizeZero
                  needTruncattailLastLine:_needTruncateLastLine
                          maxWidthCompare:_maxWidth];
    }
}

- (void) preparingFrame:(CGFloat) maxWidth
{
    _maxWidth = maxWidth;
    CGSize size = CGSizeMake(maxWidth, 0);
    BOOL updateContents = [self layoutNeedsUpdatingForContainerSize:size];
    if (updateContents == true) {
        [self layoutForContainerSize:size];
    }
}

- (bool)layoutNeedsUpdatingForContainerSize:(CGSize)containerSize
{
    if (_layoutData == nil || ABS(containerSize.width - _cachedLayoutContainerWidth) > FLT_EPSILON)
        return true;
    return false;
}

- (void)setText:(NSString *)text
{
    if (!StringCompare(_text, text))
    {
        _cachedLayoutContainerWidth = 0.0f;
        
        if (text.length != 0)
            _text = text;
        else
            _text = @" ";
    }
}

- (void)setFont:(CTFontRef)font
{
    if (_font != font)
    {
        _cachedLayoutContainerWidth = 0.0f;
        
        if (_font != NULL)
        {
            CFRelease(_font);
            _font = NULL;
        }
        
        if (font != NULL)
            _font = CFRetain(font);
    }
}

- (NSInteger) numberOfTextLine {
    if (_layoutData) {
        return _layoutData.numberOfLines;
    }
    return 0;
}

- (void)layoutForContainerSize:(CGSize)containerSize
{
    if ((_layoutData == nil || ABS(containerSize.width - _cachedLayoutContainerWidth) > FLT_EPSILON) && _text != nil)
    {
        _textCheckingResults = [Utils textCheckingResults:_text];
        _additionalAttributes = [Utils additionAttributeForUrls:_textCheckingResults];
        _layoutData = [ReusableLabel calculateLayout:_text
                                additionalAttributes:_additionalAttributes
                                 textCheckingResults:_textCheckingResults
                                                font:_font
                                           textColor:_textColor
                                           linkColor: _textColor
                                               frame:CGRectZero
                                          orMaxWidth:containerSize.width
                                               flags:_layoutFlags
                                       textAlignment:(NSTextAlignment)NSTextAlignmentLeft
                                            outIsRTL:&_isRTL
                             additionalTrailingWidth:_additionalTrailingWidth
                                             maxLine:_lineCount];
        _cachedLayoutContainerWidth = containerSize.width;
    }
    
    CGRect frame = CGRectZero;
    if (_layoutData != nil) {
        frame.size = _layoutData.size;
    }
    frame.size.width = floorf(frame.size.width);
    frame.size.height = floorf(frame.size.height);
    self.frame = frame;
}

- (NSString *)linkAtPoint:(CGPoint)point regionData:(__autoreleasing NSArray **)regionData
{
    CGRect topRegion = CGRectZero;
    CGRect middleRegion = CGRectZero;
    CGRect bottomRegion = CGRectZero;
    
    NSString *result = [_layoutData linkAtPoint:point topRegion:&topRegion middleRegion:&middleRegion bottomRegion:&bottomRegion];
    if (result != nil)
    {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        if (!CGRectIsEmpty(topRegion))
            [array addObject:[NSValue valueWithCGRect:topRegion]];
        if (!CGRectIsEmpty(middleRegion))
            [array addObject:[NSValue valueWithCGRect:middleRegion]];
        if (!CGRectIsEmpty(bottomRegion))
            [array addObject:[NSValue valueWithCGRect:bottomRegion]];
        
        if (regionData != NULL)
            *regionData = array;
    }
    
    return result;
}

@end
