//
//  DhtButtonViewModel.m
//  Cosplay No.1
//
//  Created by nguyenvandung on 10/10/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "DhtButtonViewModel.h"
#import "DhtButtonView.h"
#import "Utils.h"

@implementation DhtButtonViewModel

- (Class)viewClass {
    return [DhtButtonView class];
}

- (void)_updateViewStateIdentifier {
    self.viewStateIdentifier = [[NSString alloc] initWithFormat:@"ButtonView/%lx/%lx/%@/%lx/%lx", (long)_backgroundImage, (long)_highlightedBackgroundImage, _title, (long)_font, (long)_image];
}

- (void)bindViewToContainer:(UIView *)container viewStorage:(DhtViewStorage *)viewStorage {
    [self _updateViewStateIdentifier];
    [super bindViewToContainer:container viewStorage:viewStorage];
    DhtButtonView *view = (DhtButtonView *)[self boundView];
    if (!StringCompare(view.viewStateIdentifier, self.viewStateIdentifier)) {
        [view setBackgroundImage:_backgroundImage];
        [view setHighlightedBackgroundImage:_highlightedBackgroundImage];
        [view setTitle:_title];
        [view setTitleFont:_font];
        [view setImage:_image];
        view.extendedEdges = _extendedEdges;
        [view setTitleColor:self.titleColor?self.titleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    view.needHightLigth = _modernHighlight;
    
    [view addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
}

- (void) unbindView:(DhtViewStorage *)viewStorage {
    DhtButtonView *view = (DhtButtonView *)[self boundView];
    if (view) {
        [view removeTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    }
    self.action = nil;
    [super unbindView:viewStorage];
}

- (void) buttonAction {
    if (self.action) {
        self.action();
    }
}

- (void)setTitle:(NSString *)title {
    _title = title;
    _possibleTitles = nil;
    
    if ([self boundView] != nil) {
        [(DhtButtonView *)[self boundView] setTitle:_title];
    }
}

- (void)setPossibleTitles:(NSArray *)possibleTitles {
    _possibleTitles = possibleTitles;
    _title = nil;
}

- (void)setImage:(UIImage *)image {
    _image = image;
    if ([self boundView] != nil)
        [(DhtButtonView *)[self boundView] setImage:_image];
}

- (void)setBackgroundImage:(UIImage *)backgroundImage {
    _backgroundImage = backgroundImage;
    if ([self boundView] != nil)
        [(DhtButtonView *)[self boundView] setBackgroundImage:_backgroundImage];
}

- (void)layoutForContainerSize:(CGSize)containerSize {
    CGSize textSize = CGSizeMake(_titleInset.left + _titleInset.right, 0.0f);
    if (_possibleTitles.count != 0) {
        NSString *bestMatchTitle = nil;
        CGSize bestMatchSize = CGSizeZero;
        for (NSString *title in _possibleTitles) {
            bestMatchTitle = title;
            bestMatchSize = [Utils sizeForText:title boundSize:containerSize option:NSStringDrawingUsesLineFragmentOrigin lineBreakMode:NSLineBreakByWordWrapping font:_font];
            if (bestMatchSize.width + textSize.width <= containerSize.width)
                break;
        }
        _title = bestMatchTitle;
        if ([self boundView] != nil)
            [(DhtButtonView *)[self boundView] setTitle:_title];
        
        textSize.width += bestMatchSize.width;
        textSize.height += bestMatchSize.height;
    } else {
        
        CGSize titleSize = [Utils sizeForText:_title boundSize:containerSize option:NSStringDrawingUsesLineFragmentOrigin lineBreakMode:NSLineBreakByWordWrapping font:_font];
        textSize.width += titleSize.width;
        textSize.height += titleSize.height;
    }
    
    CGRect frame = self.frame;
    textSize.width = MAX(textSize.width, _backgroundImage.size.width);
    frame.size = CGSizeMake(MIN(textSize.width, containerSize.width), _backgroundImage.size.height);
    self.frame = frame;
}

- (void)drawInContext:(CGContextRef)__unused context {
    if (!self.skipDrawInContext && self.alpha > FLT_EPSILON) {
        if (_backgroundImage != nil)
            [_backgroundImage drawInRect:self.bounds blendMode:kCGBlendModeNormal alpha:1.0f];
        if (_image != nil) {
            CGRect bounds = self.bounds;
            CGSize imageSize = _image.size;
            [_image drawInRect:CGRectMake(floorf((bounds.size.width - imageSize.width) / 2.0f), floorf((bounds.size.height - imageSize.height) / 2.0f), imageSize.width, imageSize.height)];
        }
    }
}

@end
