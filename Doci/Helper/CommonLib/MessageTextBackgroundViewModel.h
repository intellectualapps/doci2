//
//  MessageTextBackgroundViewModel.h
//  MtoM
//
//  Created by nguyen van dung on 12/15/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "DhtViewModel.h"
#import "DhtImageViewModel.h"
typedef NS_ENUM(NSInteger,TextMessageBackgroundType) {
    TextMessageBackgroundIncoming = 0,
    TextMessageBackgroundOutgoing = 1
};
@interface MessageTextBackgroundViewModel : DhtImageViewModel
@property (nonatomic, assign) BOOL  partialMode;
- (instancetype)initWithType:(TextMessageBackgroundType)type;
- (void)setHighlightedIfBound;
- (void)clearHighlight;
@end
