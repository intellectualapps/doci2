//
//  ChatUtilities.h
//  kids_taxi
//
//  Created by Nguyen Van Dung on 5/9/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utils.h"
#import "ReusableLabel.h"

@interface ChatUtilities : NSObject
+ (NSTimeInterval)lastMessageTimUpdateStatus;
+ (void)saveLastTimeUpdateMessageStatus:(NSTimeInterval)time;
+ (NSString *)cleanInternationalPhone:(NSString *)phone forceInternational:(bool)forceInternational;
+ (int)defaultFlag;
@end
