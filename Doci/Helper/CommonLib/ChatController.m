//
//  ChatController.m
//  kids_taxi
//
//  Created by Nguyen Van Dung on 5/7/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import "ChatController.h"
static const char *conversationQueue = "com.mtm.conversationQeueue";

@implementation ChatController
static dispatch_queue_t messageQueue() {
    static dispatch_queue_t queue = NULL;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      queue = dispatch_queue_create(conversationQueue, 0);
                      dispatch_queue_set_specific(queue, conversationQueue, (void *)conversationQueue, NULL);
                  });
    return queue;
}

static bool isMessageQueue() {
    return dispatch_get_specific(conversationQueue) == conversationQueue;
}


static void dispatchOnMessageQueue(dispatch_block_t block, bool synchronous) {
    if (block == NULL)
        return;
    
    if (dispatch_get_specific(conversationQueue) == conversationQueue)
        block();
    else
    {
        if (synchronous)
            dispatch_sync(messageQueue(), block);
        else
            dispatch_async(messageQueue(), block);
    }
}

+ (bool)isMessageQueue {
    return isMessageQueue();
}

+ (void)dispatchOnMessageQueue:(dispatch_block_t)block {
    dispatchOnMessageQueue(block, false);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _items = [[NSMutableArray alloc] init];
}
@end