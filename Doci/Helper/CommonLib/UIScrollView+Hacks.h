//
//  UIScrollView+Hacks.h
//  ChatPro
//
//  Created by dungnv9 on 3/9/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UIScrollView(DhtHacks)
- (void)stopScrollingAnimation;
@end
