//
//  DhtButtonView.h
//  Cosplay No.1
//
//  Created by nguyenvandung on 10/10/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseView.h"
@interface DhtButtonView : UIButton<BaseViewProtocol>
{
    
}
@property (nonatomic) UIEdgeInsets extendedEdges;
@property (nonatomic) bool needHightLigth;

- (void)setBackgroundImage:(UIImage *)backgroundImage;
- (void)setHighlightedBackgroundImage:(UIImage *)highlightedBackgroundImage;
- (void)setTitle:(NSString *)title;
- (void)setTitleFont:(UIFont *)titleFont;
- (void)setImage:(UIImage *)image;
@end
