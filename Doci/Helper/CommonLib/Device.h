//
//  Device.h
//  MtoM
//
//  Created by nguyen van dung on 12/11/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Device : NSObject

+ (bool) isWidescreen;
+ (CGSize)screenSizeMessageForInterfaceOrientation:(UIInterfaceOrientation)orientation;
+ (CGSize)screenSizeForInterfaceOrientation:(UIInterfaceOrientation)orientation;
@end
