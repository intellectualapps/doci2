//
//  DhtViewModel.m
//  Cosplay No.1
//
//  Created by nguyenvandung on 9/29/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "DhtViewModel.h"
#import "DhtViewStorage.h"
#import "MessageClone.h"

@interface DhtViewModel()
{
    UIView<BaseViewProtocol> *_view;
    
    NSString *_viewIdentifier;
    NSMutableArray *_submodels;
}
@end
@implementation DhtViewModel

- (void) dealloc {
}

- (Class)viewClass {
    return nil;
}

- (void) updateMediaAvaibility {
    
}

- (void)layoutForContainerSize:(CGSize) containerSize {
    
}

- (CGRect)bounds {
    return CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

- (void) setHasNoView {
    _modelFlags.hasNoView = true;
}

- (void) setSkipDrawInContext:(BOOL)skip {
    _modelFlags.skipDrawInContext = skip;
}

- (UIView<BaseViewProtocol> *)_dequeueView:(DhtViewStorage *)viewStorage {
    if (_viewIdentifier == nil)
        _viewIdentifier = NSStringFromClass([self viewClass]);
    
    UIView<BaseViewProtocol> *view = [viewStorage dequeueViewWithIdentifier:_viewIdentifier viewStateIdentifier:_viewStateIdentifier];
    if (view == nil) {
        view = [[[self viewClass] alloc] init];
        view.layer.opacity =1;
        [view setViewIdentifier:_viewIdentifier];
    }
    return view;
}

- (void)drawInContext:(CGContextRef)context {
    if (_modelFlags.skipDrawInContext || _hidden)
        return;
    
    [self drawSubmodelsInContext:context];
}

- (void)drawSubmodelsInContext:(CGContextRef)context {
    for (DhtViewModel *submodel in self.submodels) {
        CGRect frame = submodel.frame;
        CGContextTranslateCTM(context, frame.origin.x, frame.origin.y);
        [submodel drawInContext:context];
        CGContextTranslateCTM(context, -frame.origin.x, -frame.origin.y);
    }
}

- (UIView<BaseViewProtocol> *)boundView {
    return _view;
}

- (bool)skipDrawInContext {
    return _modelFlags.skipDrawInContext;
}

- (void) setUserInteraction:(BOOL)userInteraction {
    _modelFlags.viewUserInteractionDisabled = !userInteraction;
}

- (void)bindViewToContainer:(UIView *)container viewStorage:(DhtViewStorage *)viewStorage {
    
    if (!_modelFlags.hasNoView)
        _view = [self _dequeueView:viewStorage];
    
    if (_view != nil || _modelFlags.hasNoView) {
        if (!_modelFlags.hasNoView) {
            [container addSubview:_view];
            [_view setFrame:_frame];
            _view.layer.opacity =1;
            //_view.userInteractionEnabled = !_modelFlags.viewUserInteractionDisabled;
        }
        
        if (!_modelFlags.disableSubmodelAutomaticBinding) {
            for (DhtViewModel *submodel in self.submodels) {
                [submodel bindViewToContainer:_modelFlags.hasNoView ? container : _view viewStorage:viewStorage];
            }
        }
    }
}

- (void)unbindView:(DhtViewStorage *)viewStorage {
    if (!_modelFlags.disableSubmodelAutomaticBinding) {
        for (DhtViewModel *submodel in self.submodels) {
            [submodel unbindView:viewStorage];
        }
    }
    
    if (_view != nil) {
        [viewStorage enqueueView:_view];
        [_view removeFromSuperview];
        _view = nil;
    }
}

- (void)setFrame:(CGRect)frame {
    _frame = frame;
    
    if (_view != nil)
        [_view setFrame:_frame];
}
- (NSArray *)submodels {
    return _submodels;
}

- (void)removeSubmodel:(DhtViewModel *)model viewStorage:(DhtViewStorage *)viewStorage {
    if (model == nil)
        return;
    
    if ([_submodels containsObject:model])
    {
        if (!_modelFlags.disableSubmodelAutomaticBinding)
            [model unbindView:viewStorage];
        [_submodels removeObject:model];
    }
}

- (void)moveViewToContainer:(UIView *)container
{
    if (!_modelFlags.hasNoView)
    {
        if (_view != nil)
            [container addSubview:_view];
    }
    else
    {
        if (!_modelFlags.disableSubmodelAutomaticBinding)
        {
            for (DhtViewModel *submodel in self.submodels)
            {
                [submodel moveViewToContainer:container];
            }
        }
    }
}

- (void)addSubmodel:(DhtViewModel *)model {
    if (model == nil)
        return;
    
    if (_submodels == nil)
        _submodels = [[NSMutableArray alloc] init];
    
    [_submodels addObject:model];
}

- (void) updateEventTagText:(NSString *)tag {
    
}

- (void) updateWithNewestContent:(id)content {
    
}

- (void) updateReportStatus:(BOOL)isReported {
    
}

- (void) updateFirstItem:(id)model viewStorage:(DhtViewStorage *)storage {
    
}


- (void) updateCommentCountInfo:(NSString *)pointText viewStorage:(DhtViewStorage *)viewStorage {
    
}

- (void)updateMessage:(MessageClone *)msg viewStorage:(DhtViewStorage *)viewContext {
    
}

- (void)relativeBoundsUpdated:(CGRect)__unused bounds {
}

- (CGRect)effectiveContentFrame {
    return CGRectZero;
}

- (void)setTemporaryHighlighted:(bool)__unused temporaryHighlighted viewStorage:(DhtViewStorage *)__unused viewStorage {
}

- (void)updateItemInfo:(GenericInfo *)msg viewStorage:(DhtViewStorage *)viewStorage {
    
}

@end
