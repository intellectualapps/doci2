//
//  DhtImageView.m
//  MtoM
//
//  Created by nguyen van dung on 12/16/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "DhtImageView.h"
@interface DhtImageView()
{
    
}
@property (nonatomic, strong) NSString * viewIdentifier;
@end
@implementation DhtImageView

@synthesize viewIdentifier = _viewIdentifier;

- (id)init
{
    self = [super initWithFrame:CGRectZero];
    if (self){
        self.layer.opacity = 1;
        
    }
    return self;
}
- (void)willBecomeRecycled
{
    [super willBecomeRecycled];
    self.viewStateIdentifier =nil;
    self.backgroundColor = nil;
    self.layer.contents = nil;
    self.image = nil;
    [self reset];
}

- (void) setViewIdentifier:(NSString *)viewIdentifier
{
    _viewIdentifier = viewIdentifier;
}

- (NSString *)viewStateIdentifier {
    return [[NSString alloc] initWithFormat:@"DhtImageView/%lx", (long)self.image];
}


- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (!UIEdgeInsetsEqualToEdgeInsets(_extendedEdges, UIEdgeInsetsZero))
    {
        CGRect extendedFrame = self.bounds;
        
        extendedFrame.origin.x -= _extendedEdges.left;
        extendedFrame.size.width += _extendedEdges.left;
        extendedFrame.origin.y -= _extendedEdges.top;
        extendedFrame.size.height += _extendedEdges.top;
        
        extendedFrame.size.width += _extendedEdges.right;
        extendedFrame.size.height += _extendedEdges.bottom;
        
        if (CGRectContainsPoint(extendedFrame, point))
            return self;
    }
    
    return [super hitTest:point withEvent:event];
}

@end
