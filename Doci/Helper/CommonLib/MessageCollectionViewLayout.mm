//
//  MessageCollectionViewLayout.m
//  MtoM
//
//  Created by nguyen van dung on 12/11/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "MessageCollectionViewLayout.h"
#import "CollectionCell.h"
#import "MessageCollectionView.h"
#import "ChatUtilities.h"
#import "MessageItem.h"
#import "Message.h"

@interface MessageCollectionViewLayout()
{
    int _dateOffset;
    NSMutableArray *_layoutAttributes;
    CGSize _contentSize;
    std::vector<DhtDecorationViewAttrubutes> _decorationViewAttributes;
    
    NSMutableArray *_insertIndexPaths;
    NSMutableArray *_deleteIndexPaths;
}
@end


@implementation MessageCollectionViewLayout


- (id) init {
    self = [super init];
    if (self) {
        _layoutAttributes = [[NSMutableArray alloc]init];
        _dateOffset = 0;
        _needDateDecorationView = true;
    }
    
    return self;
}

- (void) prepareForCollectionViewUpdates:(NSArray *)updateItems {
    [super prepareForCollectionViewUpdates:updateItems];
    _deleteIndexPaths = [NSMutableArray new];
    _insertIndexPaths = [NSMutableArray new];
    for (UICollectionViewUpdateItem *update in updateItems) {
        if (update.updateAction == UICollectionUpdateActionDelete) {
            [_deleteIndexPaths addObject:update.indexPathBeforeUpdate];
        } else if (update.updateAction == UICollectionUpdateActionInsert) {
            [_insertIndexPaths addObject:update.indexPathAfterUpdate];
        }
    }
}

- (void)finalizeCollectionViewUpdates {
    [super finalizeCollectionViewUpdates];
    // release the insert and delete index paths
    _deleteIndexPaths = nil;
    _insertIndexPaths = nil;
}

- (UICollectionViewLayoutAttributes *)initialLayoutAttributesForAppearingItemAtIndexPath:(NSIndexPath *)itemIndexPath {
    // Must call super
    UICollectionViewLayoutAttributes *attributes = [super initialLayoutAttributesForAppearingItemAtIndexPath:itemIndexPath];
    
    if ([_insertIndexPaths containsObject:itemIndexPath]) {
        // only change attributes on inserted cells
        if (!attributes)
            attributes = [self layoutAttributesForItemAtIndexPath:itemIndexPath];
        
        attributes = [attributes copy];
        
        attributes.transform3D = CATransform3DMakeTranslation(0.0f, -attributes.frame.size.height - 4.0f, 0.0f);
        
        if (itemIndexPath.item != 0 || iosMajorVersion() < 7 || self.collectionView.contentOffset.y < -self.collectionView.contentInset.top - FLT_EPSILON) {
            attributes.alpha = 0.0f;
        } else {
            attributes.alpha = 1.0f;
            attributes.bounds = CGRectMake(0, 0, attributes.frame.size.width, 24.0);
        }
    }
    
    return attributes;
}

// Note: name of method changed
// Also this gets called for all visible cells (not just the deleted ones) and
// even gets called when inserting cells!
- (UICollectionViewLayoutAttributes *)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath {
    // So far, calling super hasn't been strictly necessary here, but leaving it in
    // for good measure
    UICollectionViewLayoutAttributes *attributes = [super finalLayoutAttributesForDisappearingItemAtIndexPath:itemIndexPath];
    
    if ([_deleteIndexPaths containsObject:itemIndexPath]) {
        // only change attributes on deleted cells
        if (!attributes) {
            //attributes = [self layoutAttributesForItemAtIndexPath:itemIndexPath];
        }
        
        attributes = [attributes copy];
        
        // Configure attributes ...
        attributes.alpha = 0.0;
    }
    
    return attributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= (int)_layoutAttributes.count)
        return nil;
    
    return _layoutAttributes[indexPath.row];
}

- (void) prepareLayout {
    [_layoutAttributes removeAllObjects];
    _decorationViewAttributes.clear();
    __block CGFloat contentHeight = 0.0f;
    dispatch_block_t block = ^
    {
        NSArray *atttributeds = [self layoutAttributesForItems:[(id<MessageViewLayoutDelegate>)self.collectionView.delegate items]
                                                containerWidth:self.collectionView.bounds.size.width
                                                     maxHeight:FLT_MAX
                                      decorationViewAttributes:&_decorationViewAttributes
                                                 contentHeight:&contentHeight
                                        needDateDecorationView:self.needDateDecorationView];
        [_layoutAttributes addObjectsFromArray:atttributeds];
    };
    
    if (_animateLayout) {
        [UIView animateWithDuration:0.3 * 0.7 delay:0 options:0 animations:^
         {
             block();
         } completion:nil];
    }
    else
        block();
    
    _contentSize = CGSizeMake(self.collectionView.bounds.size.width, contentHeight);
    if (_contentSize.height < self.collectionView.bounds.size.height){
        _contentSize.height = self.collectionView.bounds.size.height;
    }
    std::sort(_decorationViewAttributes.begin(), _decorationViewAttributes.end(), DhtDecorationViewAttrubutesComparator());
}

- (bool)hasLayoutAttributes {
    return _contentSize.height > FLT_EPSILON;
}

- (std::vector<DhtDecorationViewAttrubutes> *)allDecorationViewAttributes {
    return &_decorationViewAttributes;
}

- (NSArray *)layoutAttributesForItems:(NSArray *)items containerWidth:(CGFloat)containerWidth maxHeight:(CGFloat)maxHeight decorationViewAttributes:(std::vector<DhtDecorationViewAttrubutes> *)decorationViewAttributes contentHeight:(CGFloat *)contentHeight needDateDecorationView: (BOOL)needDate {
    return [MessageCollectionViewLayout layoutAttributesForItems:items containerWidth:containerWidth maxHeight:maxHeight dateOffset:_dateOffset decorationViewAttributes:decorationViewAttributes contentHeight:contentHeight needDateDecorationView:needDate];
}

static inline CGFloat addDate(CGFloat currentHeight, CGFloat containerWidth, int date,bool isShowFull, std::vector<DhtDecorationViewAttrubutes> *pAttributes) {
    if (pAttributes != NULL)
        pAttributes->push_back((DhtDecorationViewAttrubutes){.index = date, .frame = CGRectMake(0, currentHeight, containerWidth, 27.0f), .isShowFull = isShowFull});
    
    return 27.0f;
}

- (CGSize)collectionViewContentSize {
    return _contentSize;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (UICollectionViewLayoutAttributes *attributes in _layoutAttributes) {
        if (!CGRectIsNull(CGRectIntersection(rect, attributes.frame)))
            [array addObject:attributes];
    }
    
    return array;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)__unused newBounds {
    return false;
}


+ (NSArray *)layoutAttributesForItems:(NSArray *)items
                       containerWidth:(CGFloat)containerWidth
                            maxHeight:(CGFloat)maxHeight
                           dateOffset:(int)dateOffset
             decorationViewAttributes:(std::vector<DhtDecorationViewAttrubutes> *)decorationViewAttributes
                        contentHeight:(CGFloat *)contentHeight
               needDateDecorationView:(BOOL)needDate {
    NSMutableArray *layoutAttributes = [[NSMutableArray alloc] init];
    
    CGFloat bottomInset = 0.0f;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        bottomInset = 14.0f;
    else
        bottomInset = 14.0f;
    
    CGFloat currentHeight = bottomInset;
    int lastMessageDay = INT_MIN;
    
    int count = (int)items.count;
    int index = 0;
    
    bool lastCollapse = false;
    bool lastInsideUnreadRange = false;
    
    bool unreadRangeIsEmpty = true;
    NSInteger lastYear = 0;
    NSInteger dateCount = 0;
    NSTimeInterval checkingTime = -1;
    for (index = 0; index < count; index++) {
        GenericItem *messageItem = items[index];
        NSInteger mYear = [Utils yearOfTimeInterval:messageItem.contentInfo.createdDate];
        messageItem.isShowFull = NO;
        if (checkingTime == -1) {
            messageItem.isShowFull = YES;
            lastYear = [Utils yearOfTimeInterval:messageItem.contentInfo.createdDate];
            checkingTime = messageItem.contentInfo.createdDate;
        } else {
            if (mYear != lastYear) {
                messageItem.isShowFull = YES;
                lastYear = mYear;
            } else {
                checkingTime = messageItem.contentInfo.createdDate;
            }
        }
        int currentMessageDay = (((int)messageItem.contentInfo.createdDate) + dateOffset) / (24 * 60 * 60);
        if (currentMessageDay > 0 && lastMessageDay > 0 && lastMessageDay != INT_MIN && currentMessageDay != lastMessageDay) {
            if (needDate) {
                currentHeight += addDate(currentHeight, containerWidth, lastMessageDay,messageItem.isShowFull , decorationViewAttributes);
                dateCount += 1;
            }
        }
        
        lastMessageDay = currentMessageDay;
        
        
        if (index + 1 < count)
        {
            GenericItem *nextItem = items[index + 1];
            
            int nextMessageDay = (((int)nextItem.contentInfo.createdDate) + dateOffset) / (24 * 60 * 60);
            if (lastMessageDay != INT_MIN && nextMessageDay != lastMessageDay)
                lastCollapse = false;
            else
            {
                lastCollapse = [nextItem collapseWithItem:messageItem forContainerSize:CGSizeMake(containerWidth, 0.0f)];
                if (lastCollapse && !unreadRangeIsEmpty)
                {
                    bool nextInsideUnreadRange = false;
                    if (lastInsideUnreadRange && !nextInsideUnreadRange)
                        lastCollapse = false;
                }
            }
            
        }
        
        CGSize itemSize = [messageItem sizeForContainerSize:CGSizeMake(containerWidth, 0.0f)];
        
        UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
        attributes.frame = CGRectMake(0, currentHeight, itemSize.width, itemSize.height);
        [layoutAttributes addObject:attributes];
        
        currentHeight += itemSize.height;
        if (currentHeight >= maxHeight)
            break;
    }
    
    if (lastMessageDay != INT_MIN && index == (int)items.count) {
        if (needDate) {
            currentHeight += addDate(currentHeight, containerWidth, lastMessageDay,1, decorationViewAttributes);
        }
    }
    
    currentHeight += 4.0f;
    
    if (contentHeight != NULL)
        *contentHeight = currentHeight;
    
    return layoutAttributes;
}


@end
