//
//  DhtViewContext.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 1/31/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class DhtViewStorage;
@class DhtHandle;

typedef void(^MessageContextFailAction)(NSString *msgIdentifier);
typedef void(^ShowMenuForMessage)(NSString *msgIdentifier);
typedef void(^OpenUrlAction)(NSString *url);

@interface DhtViewContext : NSObject
@property (nonatomic, copy) MessageContextFailAction failAction;
@property (nonatomic, copy) ShowMenuForMessage showMenuAction;
@property (nonatomic, copy) OpenUrlAction openURlAction;
@property (nonatomic, strong) DhtViewStorage    *viewStorage;
@property (nonatomic, strong) DhtHandle *handler;
@property (nonatomic) float     containerWidth;
@property (nonatomic, strong) UIImage *defaultAvatar;
@property (nonatomic, assign) BOOL needShowRealAvatar;
@end
