
//
//  YearInfo.m
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "YearInfo.h"
#import "MonthInfo.h"
@interface YearInfo()
@property (nonatomic, strong) NSMutableArray    *months;
@end
@implementation YearInfo
- (instancetype)init {
    self = [super init];
    if (self) {
        _months = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)addMonth:(MonthInfo *)info{
    [_months addObject:info];
}
@end
