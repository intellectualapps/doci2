//
//  DhtMessageNotificationLabel.m
//  Fonext
//
//  Created by nguyen van dung on 4/23/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtMessageNotificationLabel.h"
#import "Utils.h"

@implementation DhtMessageNotificationLabel

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines {
    CGRect rect = [super textRectForBounds:bounds limitedToNumberOfLines:numberOfLines];
    rect.origin.y = 0.0f;
    return rect;
}

- (void)drawTextInRect:(CGRect)__unused rect {
    if (iosMajorVersion() < 7) {
        [super drawTextInRect:[self textRectForBounds:self.bounds limitedToNumberOfLines:2]];
    } else {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 1;
        style.lineBreakMode = NSLineBreakByWordWrapping;
        
        NSDictionary *attributes = @{
                                     NSParagraphStyleAttributeName: style,
                                     NSFontAttributeName: self.font,
                                     NSForegroundColorAttributeName: self.textColor
                                     };
        
        [self.text drawWithRect:self.bounds options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil];
    }
}
@end
