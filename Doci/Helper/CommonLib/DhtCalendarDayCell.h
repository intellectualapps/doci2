//
//  DhtCalendarDayCell.h
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DayInfo;
@class DhtCalendarDayCell;
@class DhtCalendarContentView;
@protocol DhtCalendardayCellDelegate <NSObject>
- (void)didSelectDayView:(DhtCalendarDayCell *)dayView;
- (UIView *)supplementViewForBound:(CGRect)bound;
- (UIColor *)dayInPastBackgroundColor;
@end
@interface DhtCalendarDayCell : UIView
@property (nonatomic, strong, readonly) DayInfo *dayInfo;
@property (nonatomic) id<DhtCalendardayCellDelegate> delegate;
- (void)displayContent:(DayInfo *)info;
- (void)addSupplementaryView:(UIView *)view;
- (void)removeSupplementaryView;
- (void)addSelectionView:(UIView *)view;
- (void)removeSelectionView;
@end
