

//
//  NSDate+Extension.m
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "NSDate+Extension.h"

@implementation MonthDateRange
@end

@implementation DateRange
@end

@implementation NSDate(calendar)

+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.year = year;
    components.month = month;
    components.day = day;
    return [[NSDate alloc] initWithTimeInterval:0 sinceDate:[calendar dateFromComponents:components]];
}

- (NSDate *)dateByAddingMonths:(NSInteger)month {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.month = month;
    return [calendar dateByAddingComponents:components toDate: self options:NSCalendarMatchNextTime];
}

- (NSDate *)firstDayOfMonth {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = 1;
    return [calendar dateFromComponents:components];
}

- (NSDate *)dateByAddingDays:(NSInteger)days {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = days;
    return [calendar dateByAddingComponents:components toDate: self options:NSCalendarMatchNextTime];
}

- (NSDate *)dateByAddMonth:(NSInteger)month {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *someDate = [cal dateByAddingUnit:NSCalendarUnitMonth value:month toDate:self options:0];
    return someDate;
}

- (NSDate *)dateByMinusDays:(NSInteger)days {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = [self day] - days;
    return [calendar dateByAddingComponents:components toDate: self options:NSCalendarMatchPreviousTimePreservingSmallerUnits];
}

- (NSInteger)numberOfDayInMonth {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSRange dayRange = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate: self];
    return dayRange.length;
}

- (NSInteger)weakday {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *component = [calendar components:NSCalendarUnitWeekday fromDate: self];
    return component.weekday;
}

- (NSInteger)year {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *component = [calendar components:NSCalendarUnitYear fromDate: self];
    return component.year;
}

- (NSInteger)day {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *component = [calendar components:NSCalendarUnitDay fromDate: self];
    return component.day;
}

- (NSInteger)month {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *component = [calendar components:NSCalendarUnitMonth fromDate: self];
    return component.month;
}

- (NSDate *)dateAtStartOfDay {
    NSDateComponents *component = [self componentsForDate];
    component.hour = 0;
    component.minute = 0;
    component.second = 0;
    return [[NSCalendar currentCalendar] dateFromComponents:component];
}

- (NSDate *)dateAtStartOfMonth {
    NSDateComponents *component = [self componentsForDate];
    component.day = 1;
    return [[NSCalendar currentCalendar] dateFromComponents:component];
}

- (NSDate *)dateAtEndOfMonth {
    NSDateComponents *component = [self componentsForDate];
    component.day = 0;
    component.month += 1;
    return [[NSCalendar currentCalendar] dateFromComponents:component];
}


- (NSDate *)dateAtEndOfDay {
    NSDateComponents *component = [self componentsForDate];
    component.hour = 23;
    component.minute = 59;
    component.second = 59;
    return [[NSCalendar currentCalendar] dateFromComponents:component];
}

- (NSDateComponents *)componentsForDate {
    NSCalendarUnit units = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfMonth | NSCalendarUnitDay;
    return  [[NSCalendar currentCalendar] components:units fromDate: self];
}

- (DateRange *)dateRange {
    NSDateComponents *component = [self componentsForDate];
    NSInteger year = component.year;
    NSInteger month = component.month;
    NSInteger weekOfMonth = component.weekOfMonth;
    NSInteger day = component.day;
    DateRange *info = [[DateRange alloc] init];
    info.year = year;
    info.month = month;
    info.weekOfMonth = weekOfMonth;
    info.day = day;
    return info;
}

- (MonthDateRange *)monthDateRange:(NSInteger)firstWeekday {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    calendar.firstWeekday = firstWeekday;
    MonthDateRange *range = [[MonthDateRange alloc] init];
    NSCalendarUnit units = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfMonth;
    NSDateComponents *component = [calendar components: units fromDate: self];
    //start of month
    component.day = 1;
    
    NSDate *startDate = [calendar dateFromComponents: component];
    
    //end of month
    component.month += 1;
    component.day -= 1;
    NSDate *endDate = [calendar dateFromComponents: component];
    
    NSRange arange = [calendar rangeOfUnit:NSCalendarUnitWeekOfMonth inUnit:NSCalendarUnitMonth forDate:self];
    NSInteger countOfWeaks = arange.length;
    range.countOfWeaks = countOfWeaks;
    range.startDate = startDate;
    range.endDate = endDate;
    return range;
}

@end
