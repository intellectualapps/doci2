//
//  DhtCalendarDataSource.h
//  kids_taxi_driver
//
//  Created by Nguyen Van Dung on 7/25/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MonthInfo;
@interface DhtCalendarDataSource : NSObject

//Array Object
+ (MonthInfo *)getMonthDaysInfoForDate:(NSDate *)date
                            starterDay:(NSInteger)starterWeekDay
                       includingDayOut:(BOOL)includeDayOut;
@end
