//
//  DhtTextViewActionViewModel.h
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/7/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "DhtViewModel.h"

@interface DhtTextViewActionViewModel : DhtViewModel
{
    
}

- (instancetype) initWithText:(NSString *)text
                  viewContext:(DhtViewContext *)context
                     fontSize:(CGFloat)fontSize
                       isBold:(BOOL) isBold
                    textColor:(UIColor *)textColor
                      maxLine:(NSInteger)maxLie
                 truncateTail:(BOOL)isTruncatetail
           textCheckingResult:(NSArray *)textCheckingResults;
- (NSInteger) numberOfTextLine;
@end
