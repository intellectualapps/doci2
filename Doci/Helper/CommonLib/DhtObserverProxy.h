//
//  DhtObserverProxy.h
//  kids_taxi
//
//  Created by Nguyen Van Dung on 3/14/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DhtObserverProxy : NSObject
@property (nonatomic) NSUInteger numberOfRunLoopPassesToDelayTargetNotifications;

- (instancetype)initWithTarget:(id)target targetSelector:(SEL)targetSelector name:(NSString *)name;
- (instancetype)initWithTarget:(id)target targetSelector:(SEL)targetSelector name:(NSString *)name object:(id)object;
@end
