//
//  ChatUtilities.m
//  kids_taxi
//
//  Created by Nguyen Van Dung on 5/9/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import "ChatUtilities.h"

@implementation ChatUtilities
+ (NSTimeInterval)lastMessageTimUpdateStatus {
    return [[NSUserDefaults standardUserDefaults] doubleForKey:@"lastUpdateTime"];
}

+ (void)saveLastTimeUpdateMessageStatus:(NSTimeInterval)time {
    [[NSUserDefaults standardUserDefaults] setDouble:time forKey:@"lastUpdateTime"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)cleanInternationalPhone:(NSString *)phone forceInternational:(bool)forceInternational {
    if (phone.length == 0)
        return @"";
    
    char buf[phone.length];
    int bufPtr = 0;
    
    bool hadPlus = false;
    NSInteger length = phone.length;
    for (int i = 0; i < length; i++) {
        unichar c = [phone characterAtIndex:i];
        if ((c >= '0' && c <= '9') || (c == '+' && !hadPlus)) {
            buf[bufPtr++] = (char)c;
            if (c == '+') {
                hadPlus = true;
            }
        }
    }
    
    NSString *result = [[NSString alloc] initWithBytes:buf length:bufPtr encoding:NSUTF8StringEncoding];
    if (forceInternational && bufPtr != 0 && buf[0] != '+') {
        result = [[NSString alloc] initWithFormat:@"+%@", result];
    }
    return result;
}

+ (int)defaultFlag {
    return  ReusableLabelLayoutMultiline | ReusableLabelLayoutHighlightLinks;
}
@end
