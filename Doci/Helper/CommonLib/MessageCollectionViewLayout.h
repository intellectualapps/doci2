//
//  MessageCollectionViewLayout.h
//  MtoM
//
//  Created by nguyen van dung on 12/11/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <UIKit/UIKit.h>
#ifdef __cplusplus
#   import <vector>
#endif


typedef struct {
    int index;
    CGRect frame;
    bool isShowFull;
} DhtDecorationViewAttrubutes;

#ifdef __cplusplus

struct DhtDecorationViewAttrubutesComparator
{
    bool operator() (const DhtDecorationViewAttrubutes &left, const DhtDecorationViewAttrubutes &right)
    {
        return (left.frame.origin.y + left.frame.size.height) < (right.frame.origin.y + right.frame.size.height);
    }
};

#endif


@interface MessageCollectionViewLayout : UICollectionViewLayout
@property (nonatomic) bool animateLayout;
@property (nonatomic) bool needDateDecorationView;
#ifdef __cplusplus
- (std::vector<DhtDecorationViewAttrubutes> *)allDecorationViewAttributes;
- (NSArray *)layoutAttributesForItems:(NSArray *)items containerWidth:(CGFloat)containerWidth maxHeight:(CGFloat)maxHeight decorationViewAttributes:(std::vector<DhtDecorationViewAttrubutes> *)decorationViewAttributes contentHeight:(CGFloat *)contentHeight needDateDecorationView: (BOOL)needDate ;
+ (NSArray *)layoutAttributesForItems:(NSArray *)items
                       containerWidth:(CGFloat)containerWidth
                            maxHeight:(CGFloat)maxHeight
                           dateOffset:(int)dateOffset
             decorationViewAttributes:(std::vector<DhtDecorationViewAttrubutes> *)decorationViewAttributes
                        contentHeight:(CGFloat *)contentHeight
               needDateDecorationView:(BOOL)needDate;
#endif

- (bool)hasLayoutAttributes;
@end

@protocol MessageViewLayoutDelegate <UICollectionViewDelegate>

- (NSArray *)items;
@end
