//
//  DhtActor.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 2/24/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DhtHandle;
@interface DhtActor : NSObject

@property (nonatomic, strong)   NSString *path;
@property (nonatomic) NSTimeInterval cancelTimeout;
@property (nonatomic) bool      cancelled;
@property (nonatomic, strong) NSString *requestQueueName;
@property (nonatomic, strong) NSDictionary *storedOptions;

/*
 Path: must be follow format if genericPath method.
 */
- (id)initWithPath:(NSString *)path;

/*
 - Implement this method to cancel any operation is running. Example download image operation.
 */
- (void)cancel;
/*
 We can do some jobs before this actor actual execute.
 */
- (void)prepare:(NSDictionary *)options;

- (void)execute:(NSDictionary *)options;
/*
 This is require to return the request path format. Check implementation to see the format.
 Example: You want to download image with url : http://cosplay.com/img1.png then the format of path like as bellow
 @"/img/@". -> actual url (@"/img/(url)")
 ActionManager will automatic parse path to get correct url
 */
+ (NSString *)genericPath;
/*
 Any actor want to execute for the task need register by this method.
 */
+ (void)registerActorClass:(Class)requestBuilderClass;

/*
 Return the actor class registered by registerActorClass: depend on path.
 */
+ (DhtActor *)requestBuilderForGenericPath:(NSString *)genericPath path:(NSString *)path;

/*
 
 */
- (void)watcherJoined:(DhtHandle *)__unused watcherHandle options:(NSDictionary *)__unused options waitingInActorQueue:(bool)__unused waitingInActorQueue;

+ (NSOperationQueue *)operationQueue;
@end
