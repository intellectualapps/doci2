//
//  ConversationInputView.h
//  ChatPro
//
//  Created by dungnv9 on 2/18/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import <UIKit/UIKit.h>
#define default_input_height 45.f
@class ConversationInputView;
@protocol ConversationInputPanelDelegate <NSObject>

- (void)inputPanelWillChangeHeight:(ConversationInputView *)inputPanel height:(CGFloat)height duration:(NSTimeInterval)duration animationCurve:(int)animationCurve;
- (CGSize)messageAreaSizeForInterfaceOrientation:(UIInterfaceOrientation)orientation;

@end
@interface ConversationInputView : UIView
@property (nonatomic, weak) id<ConversationInputPanelDelegate> delegate;

- (void)adjustForOrientation:(UIInterfaceOrientation)orientation keyboardHeight:(float)keyboardHeight duration:(NSTimeInterval)duration animationCurve:(int)animationCurve;
- (void)changeOrientationToOrientation:(UIInterfaceOrientation)__unused orientation keyboardHeight:(float)__unused keyboardHeight duration:(NSTimeInterval)__unused duration;
@end
