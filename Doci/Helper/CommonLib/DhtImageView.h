//
//  DhtImageView.h
//  MtoM
//
//  Created by nguyen van dung on 12/16/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "BaseView.h"
#import "BaseImageView.h"

@interface DhtImageView : BaseImageView<BaseViewProtocol>
@property (nonatomic) UIEdgeInsets extendedEdges;
@end
