//
//  DhtImageViewModel.m
//  MtoM
//
//  Created by nguyen van dung on 12/16/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "DhtImageViewModel.h"
#import "DhtImageView.h"
#import "Utils.h"

@implementation DhtImageViewModel
- (instancetype) initWithUrl:(NSString *)url placeholderImage:(UIImage *)placeholderImage {
    self = [super init];
    if (self) {
        _url = url;
        _placeholderImage = placeholderImage;
        _image = placeholderImage;
    }
    return self;
}
- (instancetype)initWithImage:(UIImage *)image {
    self = [super init];
    if (self != nil) {
        _image = image;
    }
    return self;
}

- (Class)viewClass {
    return [DhtImageView class];
}

- (void)_updateViewStateIdentifier {
    self.viewStateIdentifier = [[NSString alloc] initWithFormat:@"DhtImageView/%lx", (long)_image];
}

- (void)bindViewToContainer:(UIView *)container viewStorage:(DhtViewStorage *)viewStorage {
    [self _updateViewStateIdentifier];
    [super bindViewToContainer:container viewStorage:viewStorage];
    DhtImageView *view = (DhtImageView *)[self boundView];
    view.extendedEdges = _extendedEdges;
}

- (void)drawInContext:(CGContextRef)context {
    [super drawInContext:context];
    if (!self.skipDrawInContext && self.alpha > FLT_EPSILON) {
        CGRect r = self.bounds;
        [_image drawInRect:r blendMode:_blendMode alpha:1.0f];
    }
}

- (void)sizeToFit {
    CGRect frame = self.frame;
    frame.size = _image.size;
    self.frame = frame;
}

- (bool)layoutNeedsUpdatingForContainerSize:(CGSize)containerSize {
    return false;
}
@end
