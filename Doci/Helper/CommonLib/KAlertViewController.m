//
//  KAlertViewController.m
//  KidTaxiLib
//
//  Created by Nguyen Van Dung on 3/27/17.
//  Copyright © 2017 Dht. All rights reserved.
//

#import "KAlertViewController.h"

@interface KAlertViewController ()

@end

@implementation KAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (UIViewController *)findTopMostPresentedController:(UIViewController *)rootController {
  if (rootController == nil) {
    rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
  }

  UIViewController *currentVC = [rootController presentedViewController];
  if (currentVC) {
    if (currentVC.presentedViewController) {
      //recusive finding
      return [KAlertViewController findTopMostPresentedController: currentVC];
    } else {
      return  currentVC;
    }
  }
  return  nil;
}
@end
