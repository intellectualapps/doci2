//
//  Window.h
//  FTaxiLib
//
//  Created by Nguyen Van Dung on 4/3/17.
//  Copyright © 2017 Dht. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Window : NSObject
+ (UIWindow *)keyWindow;
@end
