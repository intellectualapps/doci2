//
//  DayInfo.h
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DhtCalendarDayCell.h"

@interface DayInfo : NSObject
@property (nonatomic, weak) DhtCalendarDayCell *cell;
@property (nonatomic, assign) NSInteger numberOfItem;
@property (nonatomic, assign) BOOL isDayIn;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL hasScheduleInfo;
@property (nonatomic, strong, readonly) NSDate *date;
- (instancetype)initWithDate:(NSDate *)date;
- (void)reset;
- (BOOL)isPast;
- (BOOL)isToday;
- (void)setNumberOfItem:(NSInteger)nb;
@end
