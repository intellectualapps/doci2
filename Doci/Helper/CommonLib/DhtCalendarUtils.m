//
//  DhtCalendarUtils.m
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DhtCalendarUtils.h"

@implementation DhtCalendarUtils

+ (CGSize) sizeForText:(NSString *)text
             boundSize:(CGSize)bsize
                option:(NSStringDrawingOptions)option
         lineBreakMode:(NSLineBreakMode)lineBreakMode
                  font:(UIFont *)font {
    if (!text || text.length ==0 || !font) {
        return CGSizeZero;
    }
    CGSize textSize = CGSizeZero;
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = lineBreakMode;
    
    textSize = [text boundingRectWithSize: bsize
                                  options:NSStringDrawingUsesLineFragmentOrigin
                               attributes:@{NSFontAttributeName:font,NSParagraphStyleAttributeName:paragraphStyle}
                                  context:nil].size;
    return textSize;
}


@end