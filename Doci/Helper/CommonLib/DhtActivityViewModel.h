//
//  DhtActivityViewModel.h
//  Fonext
//
//  Created by dungnv9 on 3/17/15.
//  Copyright (c) 2015 dungnv9. All rights reserved.
//

#import "DhtViewModel.h"

@interface DhtActivityViewModel : DhtViewModel

- (void) startOrStopActivity:(bool)stop;
@end
