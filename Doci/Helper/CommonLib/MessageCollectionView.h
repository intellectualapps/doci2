//
//  MessageCollectionView.h
//  MtoM
//
//  Created by nguyen van dung on 12/11/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCollectionView : UICollectionView
- (void)scrollToTopIfNeeded;
- (void) scrollToBottom:(BOOL)animated;

- (void)setDelayVisibleItemsUpdate:(bool)delay;
- (void)updateVisibleItemsNow;
- (bool)disableDecorationViewUpdates;
- (void)setDisableDecorationViewUpdates:(bool)disableDecorationViewUpdates;
- (void)updateRelativeBounds;

- (UIView *)viewForDecorationAtIndex:(int)index;
- (NSArray *)visibleDecorations;
- (void)updateDecorationAssets;
- (void)stopScrollingAnimation;
- (bool)performBatchUpdates:(void (^)(void))updates completion:(void (^)(BOOL))completion beforeDecorations:(void (^)())beforeDecorations animated:(bool)animated animationFactor:(float)animationFactor;
@end
