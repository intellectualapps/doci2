//
//  DhtButtonViewModel.h
//  Cosplay No.1
//
//  Created by nguyenvandung on 10/10/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "DhtViewModel.h"

@interface DhtButtonViewModel : DhtViewModel
@property (nonatomic) UIEdgeInsets extendedEdges;
@property (nonatomic, strong) UIImage *backgroundImage;
@property (nonatomic, strong) UIImage *highlightedBackgroundImage;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIColor *titleColor;
@property (nonatomic, strong) NSArray *possibleTitles;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic) UIEdgeInsets titleInset;
@property (nonatomic) bool modernHighlight;
@property (nonatomic, copy) void (^action)();
@end
