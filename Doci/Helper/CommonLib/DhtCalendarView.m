//
//  DhtCalendarView.m
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "DhtCalendarView.h"
#import "DhtCalendarDayCell.h"
#import "NSDate+Extension.h"
#import "MonthInfo.h"
#import "DayInfo.h"
#import "YearInfo.h"
#import "DhtCalendarContentView.h"
#import "DhtCalendarDataSource.h"

@interface DhtCalendarView()<iCarouselDataSource, iCarouselDelegate> {
    DhtCalendarDirection    direction;
    NSInteger starterWeekday;
}
@property (nonatomic, strong) MonthInfo *currentMonth;
@property (nonatomic, strong) iCarousel *swipeView;
@property (nonatomic, strong) NSMutableArray *items;
@end

@implementation DhtCalendarView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
    }
    return self;
}
- (instancetype)initWithFrame: (CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)commonInit {
    starterWeekday = 1;
    direction = DhtCalendarDirectionHorizontal;
    [self initDataSource];
    [self initSwipeView];
    self.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)initSwipeView {
    _swipeView.translatesAutoresizingMaskIntoConstraints = NO;
    _swipeView = [[iCarousel alloc] initWithFrame:self.bounds];
    _swipeView.type = iCarouselTypeLinear;
    _swipeView.delegate = self;
    _swipeView.dataSource = self;
    _swipeView.pagingEnabled = true;
    _swipeView.bounceDistance = 0.0;
    _swipeView.centerItemWhenSelected = true;
    _swipeView.perspective = 0;
    //add carousel to view
    [self addSubview:_swipeView];
}

- (void)dealloc {
    _swipeView.delegate = nil;
    _swipeView.dataSource = nil;
}

- (void)allowsScroll:(BOOL)allows {
    [_swipeView setScrollEnabled:allows];
}

- (void)updateConstraints {
    if (_swipeView && [_swipeView superview]) {
        NSMutableArray *constraints = [NSMutableArray array];
        NSLayoutConstraint *constraint =  [NSLayoutConstraint constraintWithItem:_swipeView
                                                                       attribute:NSLayoutAttributeTop
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:_swipeView.superview
                                                                       attribute:NSLayoutAttributeTop
                                                                      multiplier:1
                                                                        constant:0];
        [constraints addObject:constraint];
        constraint = [NSLayoutConstraint constraintWithItem:_swipeView
                                                  attribute:NSLayoutAttributeTrailing
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:_swipeView.superview
                                                  attribute:NSLayoutAttributeTrailing
                                                 multiplier:1
                                                   constant:0];
        [constraints addObject:constraint];
        constraint = [NSLayoutConstraint constraintWithItem:_swipeView
                                                  attribute:NSLayoutAttributeLeading
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:_swipeView.superview
                                                  attribute:NSLayoutAttributeLeading
                                                 multiplier:1
                                                   constant:0];
        [constraints addObject:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:_swipeView
                                                  attribute:NSLayoutAttributeBottom
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:_swipeView.superview
                                                  attribute:NSLayoutAttributeBottom
                                                 multiplier:1
                                                   constant:0];
        [constraints addObject:constraint];
        [NSLayoutConstraint activateConstraints:constraints];
    }
    [super updateConstraints];
}

- (void)initDataSource {
    _items = [[NSMutableArray  alloc] init];
    [self setupDataSource:[NSDate date]];;
}

- (MonthInfo *)monthDaysContent:(NSInteger)year month:(NSInteger)month {
    for (MonthInfo *info in self.items) {
        DateRange *range = [info.monthDate dateRange];
        if (range.year == year && range.month == month) {
            return info;
        }
    }
    return nil;
}

- (void)resetSelection {
    if (self.items.count > 0) {
        for (MonthInfo *info in self.items) {
            [info resetSelection];
        }
    }
}

- (DhtCalendarContentView *)currentMonthView {
    return (DhtCalendarContentView *)[_swipeView currentItemView];
}

- (NSInteger)currentMonthIndex {
    return [_swipeView currentItemIndex];
}

- (void)getWeakDayForDate:(NSDate *)date {
    MonthInfo *info = [DhtCalendarDataSource getMonthDaysInfoForDate:date starterDay:starterWeekday includingDayOut:true];
    info.monthDate = [[date dateAtStartOfMonth] dateByAddingDays: 15];
    [self.items addObject:info];
}

/*
 Foreach setup we will get content for 5 months. Current month is center
 */
- (void)setupDataSource:(NSDate *)currentDate {

    [self.items removeAllObjects];
    for (int i = 120; i >= 1; i--) {
        NSDate *pre1 = [currentDate dateByAddMonth: -i];
        [self getWeakDayForDate:pre1];
    }
    [self getWeakDayForDate:currentDate];
    for (int i = 1; i <= 120; i++) {
        NSDate *next = [currentDate dateByAddMonth: i];
        [self getWeakDayForDate:next];
    }
}

- (NSDate *)nextWeekDateFromDate:(NSDate *)date {
    NSDateComponents *component = [date componentsForDate];
    component.day += 7;
    return  [[NSCalendar currentCalendar] dateFromComponents:component];
}

- (NSInteger)weekdayForDate:(NSDate *)date {
    NSCalendarUnit unit = NSCalendarUnitWeekday;
    NSDateComponents *component = [[NSCalendar currentCalendar] components:unit fromDate:date];
    return component.weekday;
}

- (MonthInfo *)monthInfoWithDate:(NSDate *)date {
    for (MonthInfo *info in self.items) {
        if ([[info.monthDate dateAtStartOfMonth] isEqualToDate:[date dateAtStartOfMonth]]) {
            return info;
        }
    }
    return nil;
}

- (void)correctFrame {
}

- (void)autoSelectCurrentMonthForDate:(NSDate *)date animated:(BOOL)animated { 
    //Auto select current month for first display
    NSDate *today = [date dateAtStartOfMonth];
    NSInteger index = 0;
    for (MonthInfo *info in self.items) {
        if ([[info.monthDate dateAtStartOfMonth] isEqualToDate:today]) {
            break;
        }
        index += 1;
    }
    if (index < self.items.count) {
        _currentMonth = [self.items objectAtIndex:index];
        [self scrollToIndex:index animated:animated];
    }
}

- (void)scrollToIndex:(NSInteger)index animated:(BOOL)animated {
    [_swipeView scrollToItemAtIndex:index animated:animated];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _swipeView.frame = self.bounds;
    _swipeView.backgroundColor = [UIColor blueColor];
}

- (void)selectDays:(NSArray *)days ofMonth:(MonthInfo *)monthInfo {
    //. reset all select day first
    [monthInfo resetSelection];
    [monthInfo autoSelectDates:days];
    DhtCalendarContentView *contentView = (DhtCalendarContentView *)[_swipeView currentItemView];
    [contentView reloadContent:monthInfo];
}

#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return self.items.count;
}


- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    DhtCalendarContentView *monthView = (DhtCalendarContentView *)view;
    //create new view if no view is available for recycling
    if (monthView == nil) {
        monthView = [[DhtCalendarContentView alloc] initWithFrame: CGRectMake(0, 0, carousel.bounds.size.width, carousel.bounds.size.height)];
    } else {
        [[monthView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    if  (monthView) {
        monthView.backgroundColor = [UIColor  clearColor];
        [monthView setNeedsUpdateConstraints];
        if (carousel.currentItemIndex > 0 && carousel.currentItemIndex < self.items.count - 1) {
            MonthInfo *info = [self.items objectAtIndex:index];
            NSLog(@"current month %@",info.monthDate);
            _currentMonth = [info copy];
            [monthView reloadContent: _currentMonth];
            [monthView setDelegate: self.delegate];
        }
    }
    return monthView;
}

- (CATransform3D)carousel:(iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * carousel.itemWidth);
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return true;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.003f;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        default:
        {
            return value;
        }
    }
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel {
    return [[UIScreen mainScreen] bounds].size.width;
}

- (void)carouselWillBeginDragging:(iCarousel *)carousel {
    if (self.delegate) {
        [self.delegate calendarWillBeginDragging];
    }
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel {
    if (self.delegate) {
        [self.delegate didSelectedMonthView: (DhtCalendarContentView *)carousel.currentItemView];
    }
}

- (CGFloat)totalHeight {
    if ([self currentMonthView] != nil) {
        return [[self currentMonthView] totalHeight];
    }
    return 0.0;
}
@end
