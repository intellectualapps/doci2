//
//  Device.m
//  MtoM
//
//  Created by nguyen van dung on 12/11/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "Device.h"
#import "Utils.h"

@implementation Device

- (UIColor *) defaultTextColor {
    static UIColor *color;
    if (color ==nil) {
        color = [UIColor colorWithRed:81.0/255.0 green:81.0/255.0 blue:81.0/255.0f alpha:1.0f];
    }
    return color;
}

+ (CGSize)screenSizeMessageForInterfaceOrientation:(UIInterfaceOrientation)orientation {
    CGSize mainScreenSize = ScreenSize();
    CGSize size = CGSizeZero;
    if (UIInterfaceOrientationIsPortrait(orientation))
        size = CGSizeMake(mainScreenSize.width, mainScreenSize.height - 64);
    else
        size = CGSizeMake(mainScreenSize.height, mainScreenSize.width - 64);
    return size;
}

+ (CGSize)screenSizeForInterfaceOrientation:(UIInterfaceOrientation)orientation {
    CGSize mainScreenSize = ScreenSize();
    
    CGSize size = CGSizeZero;
    if (UIInterfaceOrientationIsPortrait(orientation))
        size = CGSizeMake(mainScreenSize.width, mainScreenSize.height);
    else
        size = CGSizeMake(mainScreenSize.height, mainScreenSize.width);
    
    return size;
}

+ (bool) isWidescreen {
    static bool isWidescreenInitialized = false;
    static bool isWidescreen = false;
    
    if (!isWidescreenInitialized) {
        isWidescreenInitialized = true;
        CGSize screenSize = [Device screenSizeForInterfaceOrientation:UIInterfaceOrientationPortrait];
        if (screenSize.width > 321 || screenSize.height > 481)
            isWidescreen = true;
    }
    return isWidescreen;
}

@end
