//
//  MonthInfo.m
//  DhtCalendar
//
//  Created by Nguyen Van Dung on 6/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "MonthInfo.h"
#import "DayInfo.h"
#import "NSDate+Extension.h"

@interface MonthInfo() {
    
}
@property (nonatomic, strong) NSMutableArray *days;
@end

@implementation MonthInfo
- (id)copyWithZone:(NSZone *)__unused zone {
    MonthInfo *info = [[MonthInfo alloc] init];
    info.days = [[NSMutableArray alloc] init];
    [info.days addObjectsFromArray: self.days];
    info.monthDate = [self.monthDate copy];
    info.selectedDate = [self.selectedDate copy];
    info.numberOfWeaks = self.numberOfWeaks;
    return  info;
}
- (instancetype)init {
    self = [super init];
    if (self) {
        _days = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)addDay:(DayInfo *)info {
    [_days addObject:info];
}

- (NSArray *)listOfDays {
    return self.days;
}

- (void)resetContent {
    for (DayInfo *info in self.days) {
        info.hasScheduleInfo = NO;
    }
}

- (void)resetSelection {
    for (DayInfo *info in self.days) {
        info.isSelected = NO;
    }
}

- (void)autoSelectDates:(NSArray *)dates {
    [self resetSelection];
    if (dates && dates.count > 0) {
        for (NSDate *date in dates) {
            NSDate *compareDate = [date dateAtStartOfDay];
            for (DayInfo *info in self.days) {
                NSDate *dayDate = [info.date dateAtStartOfDay];
                if ([compareDate isEqualToDate:dayDate]) {
                    info.isSelected = YES;
                } else {
                    info.isSelected = NO;
                }
            }
        }
    } else {
        DayInfo *info = [self firstDayInInMonth];
        info.isSelected = YES;
    }
}

- (DayInfo *)firstDayInInMonth {
    NSLog(@"month date :%@",self.monthDate);
    [self.days sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        DayInfo *info1 = (DayInfo *)obj1;
        DayInfo *info2 = (DayInfo *)obj2;
        return [info1.date timeIntervalSince1970] > [info2.date timeIntervalSince1970];
    }];
    for (DayInfo *info in self.days) {
        if (info.isDayIn && [[info.date dateAtStartOfMonth] timeIntervalSince1970] == [[self.monthDate dateAtStartOfMonth] timeIntervalSince1970]) {
            return info;
        }
    }
    return nil;
}
@end
