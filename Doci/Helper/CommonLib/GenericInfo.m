//
//  GenericInfo.m
//  kids_taxi_driver
//
//  Created by Nguyen Van Dung on 5/23/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import "GenericInfo.h"
#import "Utils.h"

@implementation GenericInfo

- (NSString *) localIdentifier {
    if (!_localIdentifier || _localIdentifier.length == 0) {
        _localIdentifier = [Utils generateUUID];
    }
    return _localIdentifier;
}

- (NSString *)authorName {
    return @"";
}

- (NSString *)getIdentifier {
    return  @"";
}
@end
