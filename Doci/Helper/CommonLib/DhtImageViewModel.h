//
//  DhtImageViewModel.h
//  MtoM
//
//  Created by nguyen van dung on 12/16/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "DhtViewModel.h"

@interface DhtImageViewModel : DhtViewModel
{
    NSString *_url;
}
@property (nonatomic, assign) BOOL isIncoming;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImage *placeholderImage;
@property (nonatomic) CGBlendMode blendMode;
@property (nonatomic) UIEdgeInsets extendedEdges;
- (instancetype)initWithImage:(UIImage *)image;
- (instancetype) initWithUrl:(NSString *)url placeholderImage:(UIImage *)placeholderImage;
- (bool)layoutNeedsUpdatingForContainerSize:(CGSize)containerSize;

@end
