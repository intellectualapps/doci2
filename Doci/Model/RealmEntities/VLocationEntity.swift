//
//  VLocationEntity.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 5/21/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
class VLocationEntity: Object {
    @objc dynamic var localIdentifier = ""
    @objc dynamic var locationId = ""
    @objc dynamic var name = ""
    @objc dynamic var fullAddress = ""
    @objc dynamic var city = ""
    @objc dynamic var country = ""
    @objc dynamic var countryCode = ""
    @objc dynamic var latidue: Double = 0
    @objc dynamic var longitude: Double = 0
    @objc dynamic var isHistory = false

    override class func primaryKey() -> String? {
        return "localIdentifier"
    }

    func mapFrom(info: VLocation, includeId: Bool = true) {
        if includeId {
            self.localIdentifier = info.localIdentifier
        }
        locationId = info.remoteId
        name = info.name
        fullAddress = info.fullAddress
        city = info.city
        country = info.country
        countryCode = info.countryCode
        latidue = info.coordinate.latitude
        longitude = info.coordinate.longitude
    }

    class func entityForLocationInfo(info: VLocation?) -> (VLocationEntity?, Bool) {
        guard let realm = DatabaseManager.shared.realm else {
            return (nil, false)
        }
        guard let info = info else {
            return (nil, false)
        }
        var obj = realm.object(ofType: VLocationEntity.self, forPrimaryKey: info.localIdentifier)
        var isNew = false
        if obj == nil {
            obj = VLocationEntity()
            obj?.mapFrom(info: info)
            isNew = true
        } else {
            obj?.mapFrom(info: info, includeId: false)
        }
        return (obj, isNew)
    }
}
