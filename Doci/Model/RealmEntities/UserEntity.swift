//
//  UserEntity.swift
//  Doci
//
//  Created by Nguyen Van Dung on 8/30/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class UserEntity: Object {
    @objc dynamic var id = -1
    @objc dynamic var gender = "Unspecified"
    @objc dynamic var name = ""
    @objc dynamic var phone = ""
    @objc dynamic var email = ""
    @objc dynamic var password = ""
    @objc dynamic var token = ""
    @objc dynamic var avatarPath = ""
    @objc dynamic var birthday = ""
    @objc dynamic var isEmailVerified = false
    @objc dynamic var role = -1
    @objc dynamic var address = ""
    @objc dynamic var location: VLocationEntity?
    @objc dynamic var isConnectedFacebook = false
    @objc dynamic var isConnectedGoogle = false

    override class func primaryKey() -> String? {
        return "id"
    }

    func mapFrom(user: User, inlcudeId: Bool = true) {
        if inlcudeId {
            self.id = user.id
        }
        isConnectedFacebook = user.isConnectedFacebook
        isConnectedGoogle = user.isConnectedGoogle
        gender = user.gender.rawValue
        name = user.name
        phone = user.phone
        email = user.email
        password = user.password
        token = user.token
        avatarPath = user.avatarPath
        birthday = user.birthday
        isEmailVerified = user.isEmailVerified
        role = user.role
        address = user.address
        let linfo = VLocationEntity.entityForLocationInfo(info: user.location)
        if let entity = linfo.0 {
            self.location = entity
        }
    }

    class func entityForInfo(info: User) -> (UserEntity?, Bool) {
        guard let realm = DatabaseManager.shared.realm else {
            return (nil, false)
        }
        var obj = realm.object(ofType: UserEntity.self, forPrimaryKey: info.id)
        var isNew = false
        if obj == nil {
            obj = UserEntity()
            obj?.mapFrom(user: info)
            isNew = true
        } else {
            obj?.mapFrom(user: info, inlcudeId: false)
        }
        return (obj, isNew)
    }
}
