//
//  GridItem.swift
//  CourseApp
//
//  Created by Nguyen Van Dung on 1/24/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
import IGListKit

class GridItem: NSObject {
    var itemCount = 0
    var items = [Any]() {
        didSet {
            itemCount = items.count
        }
    }

    init(items: [Any]) {
        super.init()
        self.items = items
    }

    private func computeItems() -> [GridItemInfo] {
        return [Int](1...itemCount).map { i in
            let info = GridItemInfo()
            info.title = "Main title " + String(i)
            info.subtitle = "Sub title " + String(1)
            return info
        }
    }

    override init() {
        super.init()
    }

    func search(text: String) {
//        if text.isEmpty {
//            self.items = computeItems()
//        } else {
//            var temp = [GridItemInfo]()
//            let computeItems = self.computeItems()
//            computeItems.forEach { (item) in
//                if item.title.lowercased().contains(text.lowercased()) {
//                    temp.append(item)
//                }
//            }
//            self.items = temp
//        }
    }
}

extension GridItem: ListDiffable {

    func diffIdentifier() -> NSObjectProtocol {
        return self
    }

    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        return self === object ? true : self.isEqual(object)
    }

}

