//
//  Specialties.swift
//  Doci
//
//  Created by Nguyen Van Dung on 8/1/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class Specialties: SearchTypeObject {
    var count = 0

    override func parseDict(dict: [String : Any]) {
        super.parseDict(dict: dict)
        count = dict.intForKey(key: "count")
        name = dict.stringOrEmptyForKey(key: "title")
    }
}
