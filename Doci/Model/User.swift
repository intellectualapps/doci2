//
//  User.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 5/30/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

struct  User {
    var gender: Gender = .None
    var name = ""
    var phone = ""
    var email = ""
    var password = ""
    var token = ""
    var avatarPath = ""
    var birthday = ""
    var isEmailVerified = false
    var id = -1
    var role = -1
    var address = ""
    var isConnectedFacebook = false
    var isConnectedGoogle = false
    //Cầu Giấy, Hanoi, Vietnam
    var location: VLocation?

    init(realmObj: UserEntity) {
        self.id = realmObj.id
        if let entity = realmObj.location {
            location = VLocation(realmObj: entity)
        }
        gender = Gender(rawValue: realmObj.gender) ?? .None
        name = realmObj.name
        phone = realmObj.phone
        email = realmObj.email
        password = realmObj.password
        token = realmObj.token
        avatarPath = realmObj.avatarPath
        birthday = realmObj.birthday
        isEmailVerified = realmObj.isEmailVerified
        id = realmObj.id
        role = realmObj.role
        address = realmObj.address
        isConnectedGoogle = realmObj.isConnectedGoogle
        isConnectedFacebook = realmObj.isConnectedFacebook
    }

    init(name: String,
         email: String,
         password: String,
         token: String) {
        self.name = name
        self.email = email
        self.password = password
        self.token = token
    }

    /*
     Invoked when login or signup success
     */
    static func userFromDict(dict: [String: Any], useAsLogin: Bool = true) -> User? {
        if let userDict = dict["user"] as? [String: Any] {
            var user = User(name: "", email: "", password: "", token: "")
            user.name = userDict.stringOrEmptyForKey(key: "name")
            user.id = userDict.intForKey(key: "id")
            user.role = userDict.intForKey(key: "role")
            user.isEmailVerified = userDict.boolForKey(key: "email_verified_at")
            user.email = userDict.stringOrEmptyForKey(key: "email")
            user.birthday = userDict.stringOrEmptyForKey(key: "birthday")
            user.avatarPath = userDict.stringOrEmptyForKey(key: "photo_url_small")
            user.address = userDict.stringOrEmptyForKey(key: "address")
            user.token = dict.stringOrEmptyForKey(key: "access_token")
            user.phone = userDict.stringOrEmptyForKey(key: "phone")
            if useAsLogin {
                AppDelegate.shared.loggedUser.accept(user)
                Defaults.loggedEmail.set(value: user.email)
                Defaults.userId.set(value: user.id)
            }
            return user
        }
        return nil
    }

    static func loggedUser() -> User? {
        guard let realm = DatabaseManager.shared.realm else {
            return nil
        }
        let userid = Defaults.userId.getInt()
        if userid > 0 {
            if let obj = realm.object(ofType: UserEntity.self, forPrimaryKey: userid) {
                let user = User(realmObj: obj)
                return user
            }
        }
        return nil
    }

    func save() {
        guard let realm = DatabaseManager.shared.realm else {
            return
        }
        try? realm.write {
            let info = UserEntity.entityForInfo(info: self)
            guard let entity = info.0 else {
                return
            }
            realm.add(entity, update: .all)
        }
    }
}
