//
//  FaceBookInfo.swift
//  Doci
//
//  Created by Nguyen Van Dung on 9/4/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

struct SocialInfo {
    var name = ""
    var email = ""
    var token = ""
    var id = ""
    var photo = ""
}
