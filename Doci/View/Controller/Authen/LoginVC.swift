//
//  LoginVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/26/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class LoginVC: UITableViewController {
    @IBOutlet weak var passwordTF: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTF: SkyFloatingLabelTextField!


    @IBAction func loginAction(_ sender: Any) {
        let pass = (passwordTF?.text ?? "").trim()
        let email = (emailTF?.text ?? "").trim()
        var errMsg = ""
        if email.count == 0 {
            errMsg = "error.empty.email".localized()
        } else if email.isValidEmail() == false {
            errMsg = "error.wrongformat.email".localized()
        } else if pass.count == 0 {
            errMsg = "error.empty.password".localized()
        }
//        else if pass.count < 6 {
//            errMsg = "error.wrongformat.password".localized()
//        }
        if errMsg.count > 0 {
            Alert.showAlertWithErrorMessage(errMsg)
            return
        }

        WindowManager.shared.showProgressView()
        LoginService.login(email: email, password: pass) {[weak self] (response, error) in
            WindowManager.shared.hideProgressView()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                AppDelegate.shared.showMainScreen()
                AppDelegate.shared.window?.makeToast("Login success!.")
            }
        }
    }
}
