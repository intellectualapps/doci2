//
//  ResetPasswordVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/26/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class ResetPasswordVC: UITableViewController {
    @IBOutlet weak var emailTF: SkyFloatingLabelTextField!


    @IBAction func loginAction(_ sender: Any) {
        let email = (emailTF?.text ?? "").trim()
        var errMsg = ""
        if email.count == 0 {
            errMsg = "error.empty.email".localized()
        } else if email.isValidEmail() == false {
            errMsg = "error.wrongformat.email".localized()
        }
        if errMsg.count > 0 {
            Alert.showAlertWithErrorMessage(errMsg)
            return
        }

        WindowManager.shared.showProgressView()
        ResetPasswordService.resetPassword(email: email) { (response, error) in
            WindowManager.shared.hideProgressView()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            }
        }
    }
}
