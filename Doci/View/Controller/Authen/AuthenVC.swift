//
//  AuthenVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/25/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class AuthenVC: BaseViewController {
    @IBOutlet weak var loginEmailBtn: VButton!
    @IBOutlet weak var loginFacebooBtn: UIButton!
    @IBOutlet weak var loginGoogleBtn: VButton!
    @IBOutlet weak var profressionalBtn: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()
        loginFacebooBtn?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        GIDSignIn.sharedInstance()?.uiDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showNavigation(isShow: false)
        Defaults.professional.set(value: profressionalBtn?.isOn ?? false)
    }

    @IBAction func loginEmailAction(_ sender: Any) {
        if let ctrl = UIStoryboard.authen().AuthenEmailVC as? AuthenEmailVC {
            self.navigationController?.pushViewController(ctrl, animated: true)
        }
    }

    @IBAction func loginFacebookAction(_ sender: Any) {
        self.loginFacebookAndGetInfo { (fbinfo) in
            if let info = fbinfo {
                self.callFacebookLogin(info: info, completion: { (response, error) in

                })
            } else {
                Alert.showAlertWithErrorMessage("Login facebook error!.".localized())
            }
        }
    }

    @IBAction func loginGoogleAction(_ sender: Any) {
        self.loginGoogle { (socialInfo) in
            if let info = socialInfo {
                self.callLoginGoogle(info: info, completion: { (response, error) in
                    
                })
            }
        }
    }

    @IBAction func professionalSwitchAction(_ sender: Any) {
        Defaults.professional.set(value: profressionalBtn?.isOn ?? false)
    }


    ///MARK: Google signin GUI delegate
    override func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        if let err = error {
            Alert.showAlertWithErrorMessage(err.localizedDescription)
        }
    }

}
