//
//  ProfileVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 8/23/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class ProfileVC: BaseTableViewController {
    @IBOutlet weak var avatarView: VImageView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var connectFacebookBtn: VButton!
    @IBOutlet weak var connectFacebookStatusLbl: UILabel!
    @IBOutlet weak var connectGoogleBtn: VButton!
    @IBOutlet weak var connectGoogleStatusLbl: UILabel!
    @IBOutlet weak var profressionalProfileBtn: VButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton()
        self.title = "Profile"
        if let user = AppDelegate.shared.loggedUser.value {
            nameLbl?.text = user.name
            emailLbl?.text = user.email
            phoneLbl?.text = user.phone
            genderLbl?.text = user.gender.rawValue
            checkSocialConnectStatus()
            if user.avatarPath.count > 0 {
                avatarView?.sd_setImage(with: URL.init(string: user.avatarPath), completed: nil)
            }
        }
    }

    func checkSocialConnectStatus() {
        if let user = AppDelegate.shared.loggedUser.value {
            connectFacebookStatusLbl?.text = user.isConnectedFacebook ? "Connected" : "Connect"
            connectGoogleStatusLbl?.text = user.isConnectedGoogle ? "Connected" : "Connect"
        }
    }

    func executeUpdateProfile() {
        if var user = AppDelegate.shared.loggedUser.value {
            WindowManager.shared.showProgressView()
            UpdateProfileService.updateProfile(user: user) { (response, error) in
                WindowManager.shared.hideProgressView()
                user.save()
                AppDelegate.shared.loggedUser.accept(user)
            }
        }
    }

    @IBAction func editPhotoAction(_ sender: Any) {
        changeAvatarAction()
    }

    @IBAction func profressionalBtnAction(_ sender: Any) {
    }

    @IBAction func connectGoogleAction(_ sender: Any) {
        guard var user = AppDelegate.shared.loggedUser.value else {
            return
        }
        self.loginGoogle { (socialInfo) in
            if let info = socialInfo {
                if !user.isConnectedGoogle {
                    WindowManager.shared.showProgressView()
                    ConnectGoogleService.connectGoogle(socialInfo: info, completion: {[weak self] (response, error) in
                        WindowManager.shared.hideProgressView()
                        if let err = error {
                            Alert.showAlertWithErrorMessage(err.message)
                        } else {
                            user.isConnectedGoogle = true
                            user.save()
                            AppDelegate.shared.loggedUser.accept(user)
                            self?.checkSocialConnectStatus()
                        }
                    })
                } else {
                    WindowManager.shared.showProgressView()
                    DisconnectGoogleService.disconnectGoogle(socialInfo: info, completion: {[weak self] (response, error) in
                        WindowManager.shared.hideProgressView()
                        if let err = error {
                            Alert.showAlertWithErrorMessage(err.message)
                        } else {
                            var user = AppDelegate.shared.loggedUser.value
                            user?.isConnectedGoogle = false
                            user?.save()
                            AppDelegate.shared.loggedUser.accept(user)
                            self?.checkSocialConnectStatus()
                        }
                    })
                }
            }
        }
    }

    @IBAction func connectFacebookAction(_ sender: Any) {
        guard var user = AppDelegate.shared.loggedUser.value else {
            return
        }
        self.loginFacebookAndGetInfo { (socialInfo) in
            if let info = socialInfo {
                if !user.isConnectedFacebook {
                    WindowManager.shared.showProgressView()
                    ConnectFacebookService.connectFacebook(socialInfo: info, completion: {[weak self] (response, error) in
                        WindowManager.shared.hideProgressView()
                        if let err = error {
                            Alert.showAlertWithErrorMessage(err.message)
                        } else {
                            user.isConnectedFacebook = true
                            user.save()
                            AppDelegate.shared.loggedUser.accept(user)
                            self?.checkSocialConnectStatus()
                        }
                    })
                } else {
                    WindowManager.shared.showProgressView()
                    DisconnectFacebookService.disconnectFacebook(socialInfo: info, completion: {[weak self] (response, error) in
                        WindowManager.shared.hideProgressView()
                        if let err = error {
                            Alert.showAlertWithErrorMessage(err.message)
                        } else {
                            user.isConnectedFacebook = false
                            user.save()
                            AppDelegate.shared.loggedUser.accept(user)
                            self?.checkSocialConnectStatus()
                        }
                    })
                }
            }
        }
    }

    @IBAction func editNameAction(_ sender: Any) {
        let alert = UIAlertController(title: "Edit name", message: "", preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.placeholder = AppDelegate.shared.loggedUser.value?.name ?? ""
        }

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            if let textField = alert?.textFields?[0] {
                print("Text field: \(textField.text)")
                self.nameLbl?.text = textField.text
                if let txt = textField.text, txt.count > 0 {
                    var user = AppDelegate.shared.loggedUser.value
                    user?.name = txt
                    AppDelegate.shared.loggedUser.accept(user)
                    self.executeUpdateProfile()
                }
            }
        }))

        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func editGenderAction(_ sender: Any) {
    }

    @IBAction func editPhoneAction(_ sender: Any) {
        let alert = UIAlertController(title: "Enter phone number", message: "", preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.keyboardType = .phonePad
            textField.placeholder = AppDelegate.shared.loggedUser.value?.phone ?? ""
        }

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            if let textField = alert?.textFields?[0] {
                print("Text field: \(textField.text)")
                self.phoneLbl?.text = textField.text
                if let txt = textField.text, txt.count > 0 {
                    var user = AppDelegate.shared.loggedUser.value
                    user?.phone = txt
                    AppDelegate.shared.loggedUser.accept(user)
                    self.executeUpdateProfile()
                }
            }
        }))

        self.present(alert, animated: true, completion: nil)
    }


    func changeAvatarAction() {

        let pickerController = DKImagePickerController()
        pickerController.singleSelect = true
        pickerController.showsCancelButton = true
        pickerController.sourceType = .photo

        pickerController.didSelectAssets = {[weak self] (assets: [DKAsset]) in
            if let asset = assets.first {
                asset.fetchOriginalImage(completeBlock: { (image, error) in
                    DispatchQueue.main.async {
                        if let img = image {
                            //resize image
                            var newSize = img.size
                            let percent =  newSize.width / newSize.height
                            newSize.height = 900
                            newSize.width = newSize.height * percent
                            let newImage = img.resizedImage(newSize, interpolationQuality: .low)
                            //push to server here
                            self?.avatarView?.image = newImage

                            if let data = newImage?.pngData() {
                                WindowManager.shared.showProgressView()
                                UploadPhotoService.uploadPhoto(data: data, completion: { (response, error) in
                                    WindowManager.shared.hideProgressView()
                                    if let err = error {
                                        Alert.showAlertWithErrorMessage(err .message)
                                    } else if let info = response as? UploadPhotoCompleted {
                                        if info.message.count > 0 {
                                            self?.view?.makeToast(info.message)
                                        }
                                        var user = AppDelegate.shared.loggedUser.value
                                        user?.avatarPath = info.smallPhotoUrl
                                        user?.save()
                                        AppDelegate.shared.loggedUser.accept(user)
                                        NotificationCenter.default.post(name: NotificationName.didUpdateProfile, object: nil)
                                    }
                                })
                            }
                        }
                    }
                })
            }
        }

        self.present(pickerController, animated: true) {}
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            self.editNameAction(self)
        } else if indexPath.row == 3 {
            self.editPhoneAction(self)
        }
    }
}
