//
//  FilterGenderVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/24/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
typealias ChangeFilterOptionCompletion = (_ newOption: Int) -> ()
class FilterGenderVC: BaseViewController {
    @IBOutlet weak var noFilterBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var maleBtn: UIButton!
    var completion: ChangeFilterOptionCompletion?
    var lastFilterOption = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredContentSize = CGSize(width: 280, height: 250)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let arra = [noFilterBtn,femaleBtn, maleBtn]
        arra.forEach { (btn) in
            btn?.isSelected = false
        }
        if lastFilterOption < arra.count {
            arra[lastFilterOption]?.isSelected = true
        }
    }

    @IBAction func btnAction(_ sender: Any) {
        guard let btn = sender as? UIButton else {
            return
        }
        let tag = btn.tag
        completion?(tag)
        self.dismiss(animated: true, completion: nil)
    }

}
