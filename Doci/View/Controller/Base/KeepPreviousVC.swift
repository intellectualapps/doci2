//
//  KeepPreviousVC.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 6/19/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

protocol KeepPreviousVC: NSObjectProtocol {
    var previousViewController: UIViewController? { get set }
}

var previousViewController: UIViewController?
