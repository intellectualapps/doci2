//
//  BaseTableViewController.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/16/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class BaseTableViewController: UITableViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.enableSwipeBack()
    }

    func addLeftButtonWithImage(img: UIImage?, action: Selector, target: Any) {
        guard let img = img else {
            return
        }
        let btn = UIBarButtonItem(image: img, style: .plain, target: target, action: action)
        btn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = btn
    }

    func addBackButton() {
        addLeftButtonWithImage(img: UIImage.init(named: "back"), action: #selector(backAction), target: self)
    }

    @objc func backAction() {
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
