//
//  PresentableSideMenuVC.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 6/28/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
import SideMenu
class PresentableSideMenuVC: BaseViewController, UISideMenuNavigationControllerDelegate {
    var didSetupSideMenu = false

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !didSetupSideMenu {
            didSetupSideMenu = true
            setupSideMenu()
        }
    }

    func showLeftMenu() {
        guard let controller = SideMenuManager.default.leftMenuNavigationController else {
            return
        }
        present(controller, animated: true, completion: nil)
    }

    func hideLeftMenu() {
        guard let controller = SideMenuManager.default.leftMenuNavigationController else {
            return
        }
        controller.dismiss(animated: true, completion: nil)
    }

    func setupSideMenu() {
        // Define the menus
        let storyboard = UIStoryboard.init(name: "Menu", bundle: nil)
        SideMenuManager.default.leftMenuNavigationController = storyboard.instantiateViewController(withIdentifier: "SideMenu") as? UISideMenuNavigationController

        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.default.addPanGestureToPresent(toView: self.navigationController!.navigationBar)



       let style = SideMenuPresentationStyle.menuSlideIn
        style.backgroundColor = .white
        style.presentingEndAlpha = 1
        var setting = SideMenuSettings()
        setting.pushStyle = .preserveAndHideBackButton
        setting.presentationStyle = style
        setting.statusBarEndAlpha = 0
        setting.enableSwipeGestures = true

        if let navi = SideMenuManager.default.leftMenuNavigationController {
            navi.settings = setting
        }
    }

    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }

    func sideMenuDidAppear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }

    func sideMenuWillDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }

    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
}
