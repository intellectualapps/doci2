//
//  BaseViewController.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/16/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import FBSDKCoreKit
import Foundation

class BaseViewController: UIViewController {


    var allowsSwipeBack = true

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        GUIHelper.shared.setNavigationHeight(height: self.navigationController?.navigationBar.bounds.height ?? 0)
        print("VIEWCONTROLLER : \(NSStringFromClass(self.classForCoder))")
        changeNavigationColor(tintcolor: UIColor.init(hex: 0x9F2726),
                              textColor: UIColor.white)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        enableSwipeBack()
        GUIHelper.shared.setNavigationHeight(height: self.navigationController?.navigationBar.bounds.height ?? 0)
    }

    func addBackButton() {
        addLeftButtonWithImage(img: UIImage.init(named: "back"), action: #selector(backAction), target: self)
    }

    @objc func backAction() {
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

    deinit {
        print(NSStringFromClass(self.classForCoder) + "." + #function)
    }

    func addLeftButtonWithImage(img: UIImage?, action: Selector, target: Any) {
        guard let img = img else {
            return
        }
        let btn = UIBarButtonItem(image: img, style: .plain, target: target, action: action)
        btn.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = btn
    }

    func addRightButtonWithImage(img: UIImage?, action: Selector, target: Any) {
        guard let img = img else {
            return
        }
        let btn = UIBarButtonItem(image: img, style: .plain, target: target, action: action)
        btn.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = btn
    }
}
