//
//  SearchDoctorsVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/24/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class SearchDoctorsVC: DoctorListVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Search doctors".localized().capitalized
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadData()
    }

    override func loadData() {
        guard let model = viewModel as? SearchDoctorViewModel else {
            return
        }
        WindowManager.shared.showProgressView()
        model.searchHCP { (response, error) in
            WindowManager.shared.hideProgressView()
            if let err = error {
                Alert.showAlertWithErrorMessage(err.message)
            } else {
                self.reload()
                if model.doctors.count == 0 {
                    Alert.showAlertWithErrorMessage("message.no.result".localized())
                }
            }
        }
    }
}
