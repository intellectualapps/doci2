//
//  DoctorListVC.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/24/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class DoctorListVC: BaseViewController {
    var viewModel = DoctorListViewModel()
    @IBOutlet weak var tbView: UITableView!
    var appeared = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        addBackButton()
        changeNavigationColor(tintcolor: UIColor.init(hex: 0x9F2726),
                              textColor: UIColor.white)
        addRightButtonWithImage(img: UIImage.init(named: "filter"), action: #selector(filterAction), target: self)
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !appeared {
            appeared = true
            showPlaceholder()
        }
    }

    func loadData() {
        viewModel.fake()
    }

    func showPlaceholder() {
        //to show the loader
        tbView?.reloadData()
        tbView?.showLoader()

        //to hide the loader
        tbView?.hideLoader()
    }

    func setupTableView() {
        tbView?.delegate = self
        tbView?.dataSource = self
        tbView?.tableFooterView = UIView()
        tbView?.tableHeaderView = UIView()
        tbView?.estimatedRowHeight = 100
        tbView?.rowHeight = UITableView.automaticDimension
        let xib = UINib(nibName: "DoctorListCell", bundle: nil)
        tbView?.register(xib, forCellReuseIdentifier: "DoctorListCell")
    }

    ///MARK: --content
    func filter(option: Int) {
        viewModel.filter(gender: option)
        self.reload()
    }

    func reload() {
        DispatchQueue.main.async {[weak self] in
            self?.tbView?.reloadData()
        }
    }

    @objc func filterAction() {
        self.showGenderFilter(lastOption: viewModel.lastGenderFilterOption) {[weak self] (newOption) in
            self?.viewModel.lastGenderFilterOption = newOption
            self?.filter(option: newOption)
        }
    }
}

extension DoctorListVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sInfo = viewModel.sections[section]
        return sInfo.cells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sInfo = viewModel.sections[indexPath.section]
        let rInfo = sInfo.cells[indexPath.row]
        if rInfo.reuseIdentifier.count > 0 {
            let acell = tableView.dequeueReusableCell(withIdentifier: rInfo.reuseIdentifier, for: indexPath)
            if let cell = acell as? DoctorListCell {
                if let info = rInfo.content as? Doctor {
                    cell.config(doctor: info)
                }
            }
            return acell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let sInfo = viewModel.sections[indexPath.section]
        let rInfo = sInfo.cells[indexPath.row]
        guard let doctor = rInfo.content as? Doctor else {
            return
        }
        if let controller = UIStoryboard.doctor().DoctorDetailVC as? DoctorDetailVC {
            controller.viewModel.doctor = doctor
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
