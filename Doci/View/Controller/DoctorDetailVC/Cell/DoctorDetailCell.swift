//
//  DoctorDetailCell.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/25/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class DoctorDetailCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }

    func clear() {
        titleLbl?.text = nil
        subTitleLbl?.text = nil
    }

    func config(data: Any?) {
        if let info = data as? DetailContent {
            titleLbl?.text = info.title
            subTitleLbl?.text = info.content as? String
        }
    }
}
