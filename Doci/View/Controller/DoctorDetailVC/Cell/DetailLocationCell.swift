//
//  DetailLocationCell.swift
//  Doci
//
//  Created by Nguyen Van Dung on 7/25/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class DetailLocationCell: UITableViewCell {
    var dispose = DisposeBag()
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!

    @IBOutlet weak var openMapView: VButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
        dispose = DisposeBag()
    }

    func clear() {
        titleLbl?.text = nil
        subTitleLbl?.text = nil
    }

    func config(data: Any?) {
        if let info = data as? DetailContent {
            titleLbl?.text = info.title
            if let location = info.content as? VLocation {
                subTitleLbl?.text = location.fullAddress
            }
        }
    }
}
