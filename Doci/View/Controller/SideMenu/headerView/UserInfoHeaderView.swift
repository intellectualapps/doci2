//
//  UserInfoHeaderView.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 6/29/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class UserInfoHeaderView: UITableViewHeaderFooterView {
    var dispose = DisposeBag()

    @IBOutlet var avartarView: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var actionBtn: UIButton!

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        clear()
        NotificationCenter.default.addObserver(self, selector: #selector(didUpdateProfile), name: NotificationName.didUpdateProfile, object: nil)
        self.displayUserInfo(user: AppDelegate.shared.loggedUser.value)
        AppDelegate.shared.loggedUser.asObservable()
            .subscribe(onNext: {[weak self] (user) in
                DispatchQueue.main.async {
                    self?.displayUserInfo(user: user)
                }
            }).disposed(by: rx.disposeBag)
        if let user = AppDelegate.shared.loggedUser.value {
            self.displayUserInfo(user: user)
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        dispose = DisposeBag()
        clear()
    }

    @objc func didUpdateProfile() {
        guard let user = AppDelegate.shared.loggedUser.value else {
            return
        }
        self.displayUserInfo(user: user)
    }

    func clear() {
        nameLbl?.text = nil
    }

    func displayUserInfo(user: User?) {
        guard let user = user else {
            return
        }
        nameLbl?.text = user.name.capitalized
        if user.avatarPath.count > 0 {
            avartarView?.sd_setImage(with: URL.init(string: user.avatarPath), completed: nil)
        }
    }
}
