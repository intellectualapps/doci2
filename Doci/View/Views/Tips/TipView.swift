//
//  TipView.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 6/11/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class TipView: UIView {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var closeBtn: VButton!
    @IBOutlet weak var textView: UITextView!
    var popTip: PopTip? {
        didSet {
            popTip?.shouldDismissOnTap = true
            popTip?.bubbleColor = .black
            popTip?.offset = 10
            popTip?.arrowSize = CGSize(width: 20, height: 10)
        }
    }

    class func newInstance() -> TipView? {
        return TipView.fromNib()
    }

    @IBAction func closeBtnAction(_ sender: Any) {
        popTip?.hide()
    }
}
