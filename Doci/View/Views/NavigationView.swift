//
//  NavigationView.swift
//  VNGo_ios
//
//  Created by Nguyen Van Dung on 4/22/19.
//  Copyright © 2019 Nguyen Van Dung. All rights reserved.
//

import Foundation

class NavigationView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        GUIHelper.shared.navigationHeight.asObservable()
            .subscribe(onNext: {[weak self] (height) in
                print("Status bar height: \(height)")
                self?.heightConstaint?.constant = height
            }).disposed(by: rx.disposeBag)
    }
}
